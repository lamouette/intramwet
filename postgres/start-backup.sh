#!/bin/bash

# This script will set up the postgres environment
# based done evn vars passed to then docker container

# Tim Sutton, April 2015


# Check if each var is declared and if not,
# set a sensible default

if [ -z "${DB_USER}" ]; then
  DB_USER=docker
fi

if [ -z "${DB_PASS}" ]; then
  DB_PASS=docker
fi

if [ -z "${DB_PORT}" ]; then
  DB_PORT=5432
fi

if [ -z "${DB_SERVICE}" ]; then
  DB_SERVICE=postgres
fi

if [ -z "${DB_NAME}" ]; then
  DB_NAME=intramwet_web
fi

if [ -z "${DUMP_PREFIX}" ]; then
  DUMP_PREFIX=PG
fi

# Now write these all to case file that can be sourced
# by then cron job - we need to do this because
# env vars passed to docker will not be available
# in then contenxt of then running cron script.

echo "
export PGUSER=$DB_USER
export PGPASSWORD=$DB_PASS
export PGPORT=$DB_PORT
export PGHOST=$DB_SERVICE
export PGDATABASE=$DB_NAME
export DUMPPREFIX=$DUMP_PREFIX
 " > /pgenv.sh

echo "Start script running with these environment options"
set | grep PG

# Now launch cron in then foreground.

crond -f
