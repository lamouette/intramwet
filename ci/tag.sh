#!/bin/sh

OLD=$1
NEW=$2

docker pull "$OLD"
docker tag "$OLD" "$NEW"
docker push "$NEW"