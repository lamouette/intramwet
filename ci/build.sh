#!/bin/sh

TAG=$1
DOCKERFILE=$2
DIRECTORY=$3

docker build -t "$TAG" -f "$DOCKERFILE" "$DIRECTORY"
docker push "$TAG"
