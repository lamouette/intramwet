# Mise en place du système de développement

## Docker

### Installation

Il faut installer __Docker__ et __Docker compose__. 

Pour Windows, il suffit d'installer [Docker for Windows](https://docs.docker.com/docker-for-windows/install), qui installe tout ce qu'il faut.

Pour les distributions Linux basées sur Debian, il faut installer [Docker Community Edition](https://docs.docker.com/install/linux/docker-ce/debian/) et ensuite installer [Docker compose](https://docs.docker.com/compose/install/) (Attention, pour Linux Mint les comandes sont différentes !). Il faut ensuite se rajouter au groupe `docker` et relancer son ordi:

```bash
sudo adduser nom docker
```

### Initialisation des conteneurs et de la BD

Une fois l'installation faite, il faut initialiser les conteneurs et la BD. Pour ça, j'ai fais un beau script 
d'initialisation sur Linux (RIP Amine). Avant de le lancer cependant il vous faut le fichier d'environnement avec les 
mots de passe de la base de données. Il s'agit d'un fichier nommé `.env` à placer à la racine du projet. Si vous voulez 
vous en créer un, sa structure est la suivante:

```
DB_NAME=intramwet_web
DB_USER=intramwet_web
DB_PASS=lemotdepassedelabd
DB_SERVICE=postgres
DB_PORT=5432
SECRET=unsecretpourleserveurdjango
```
Vous pouvez ensuite lancer le script d'initialisation:

```bash
sh init.sh
```

Voilà. C'est tout. Et pour Windows il faut appliquer les commandes une à une... 

### Configuration du conteneur web

Le conteneur web nécessite quelques paramètres de base (mdp BD, position des fichiers média, hôte, etc) pour 
fonctionner. Dans `./web/intramwet_config.json.example` se trouve un fichier de configuration d'exemple correspondant 
au fichier d'initialisation précédent.

```bash
cp web/intramwet_config.json.example web/intramwet_config.json
```

Maintenant que le serveur est en place, vous pouvez le démarrer et tester le site ! 
[Quelques commandes utiles ...](https://gitlab.com/lamouette/intramwet/wikis/Architecture/Docker#manipulation-et-commandes-utiles)


## IDE

- [Installation de l'IDE Pycharm](https://gitlab.com/lamouette/intramwet/wikis/Installation/Installation-de-Pycharm)
- [Importation du projet dans Pycharm](https://gitlab.com/lamouette/intramwet/wikis/Installation/Installation-de-Pycharm#importation-du-projet-dans-pycharm)
- [Mise en place du lanceur Docker compose dans Pycharm](https://gitlab.com/lamouette/intramwet/wikis/Installation/Configuration-de-l'IDE#mise-en-place-du-lanceur-de-docker-compose-dans-pycharm)
- [Configuration de l'autocomplétion](https://gitlab.com/lamouette/intramwet/wikis/Installation/Configuration-de-l'IDE#mise-en-place-de-virtualenv)
- [Mise en place de Git](https://gitlab.com/lamouette/intramwet/wikis/Installation/Configuration-de-l'IDE#mise-en-place-de-git-dans-pycharm)

# Pour plus d'infos ...

Lisez le [wiki](https://gitlab.com/lamouette/intramwet/wikis/) !