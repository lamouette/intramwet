#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

postgres_ready() {
python << END
import sys
from os import getenv

import psycopg2

try:
    psycopg2.connect(
        dbname=getenv("DB_NAME"),
        user=getenv('DB_USER'),
        password=getenv('DB_PASS'),
        host=getenv('DB_SERVICE'),
        port=getenv('DB_PORT'),
    )
except psycopg2.OperationalError:
    sys.exit(-1)
sys.exit(0)

END
}
until postgres_ready; do
  >&2 echo 'Waiting for PostgreSQL to become available...'
  sleep 1
done
>&2 echo 'PostgreSQL is available'

exec "$@"
