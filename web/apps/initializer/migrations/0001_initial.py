# Generated by Django 3.0.6 on 2020-05-09 10:38

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='InitializationJob',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('module_name', models.CharField(max_length=255, verbose_name='Name of the module')),
                ('method_name', models.CharField(max_length=80, verbose_name='Fully qualified name of the executed method, i.e. class.method')),
            ],
        ),
    ]
