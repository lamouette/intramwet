from django.core.management.base import BaseCommand
from ...initialization_handler import initialization_handler


class Command(BaseCommand):
    help = "Reinitializes the history of executed jobs."

    def add_arguments(self, parser):
        parser.add_argument('--application')
        parser.add_argument('--initializer')
        parser.add_argument('--method')

    def handle(self, *args, **kwargs):
        application_name = kwargs['application']
        class_name = kwargs['initializer']
        method_name = kwargs['method']
        if application_name:
            if class_name:
                if method_name:
                    initialization_handler.clear_jobs_for_method(application_name, class_name, method_name)
                else:
                    initialization_handler.clear_jobs_for_class(application_name, class_name)
            else:
                initialization_handler.clear_jobs_for_application(application_name)
        else:
            initialization_handler.clear_all_jobs()
