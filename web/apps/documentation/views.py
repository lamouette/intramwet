from tempfile import TemporaryFile

from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from mistune import markdown
from xhtml2pdf import pisa

from .decorators import can_access_wiki, can_manage_wiki
from .forms import DocumentationForm, CategorieForm, \
    ImageUploadForm
from .models import Document, Categorie, Modification, Image

##########################################################################
#                                                                        #
# Gestion des catégories de documents                                    #
#                                                                        #
##########################################################################
from apps.conf.utils import media_response


@can_manage_wiki
def manage_categories(request) -> HttpResponse:
    """ Permet de créer et gérer les catégories

    :param request:
    :return:
    """
    categories = Categorie.objects.all()
    if request.method == "POST":
        form = CategorieForm(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = CategorieForm()
    return render(request, 'manage_categories.html',
                  {'form': form, 'categories': categories})


@can_manage_wiki
def modify_category(request, uuid: int) -> HttpResponse:
    """ Interface de modification d'une catégorie

    :param request:
    :param uuid: id de la catégorie à modifier
    :return:
    """
    categorie = get_object_or_404(Categorie, id=uuid)
    if request.method == 'POST':
        form = CategorieForm(request.POST, instance=categorie)
        if form.is_valid():
            form.save()
            return JsonResponse({'success': True})
        return JsonResponse({'success': False})
    form = CategorieForm(instance=categorie)
    return render(request, 'edit_category.html', {'form': form, 'id': uuid})


@can_manage_wiki
def delete_category(request, uuid: int) -> HttpResponse:
    """ Permet de supprimer une catégorie (si elle existe)

    :param request:
    :param uuid:
    :return:
    """
    categorie = get_object_or_404(Categorie, id=uuid)
    categorie.delete()
    return redirect('gestion_categories')


##########################################################################
#                                                                        #
# Gestion des documents                                                  #
#                                                                        #
##########################################################################

@can_access_wiki
def display_all(request) -> HttpResponse:
    """ Affiche tous les documents que l'utilisateur a le droit de voir (rangés par catégorie)

    :param request:
    :return:
    """
    documents = Document.objects.all().order_by('categorie', 'titre')
    return render(request, 'display_all.html', {'documents': documents})


@can_access_wiki
def display_document(request, uuid: int) -> HttpResponse:
    """ Affiche un document si l'utilisateur a le droit de le voir

    :param request:
    :param uuid: id du document à afficher
    :return:
    """
    document = get_object_or_404(Document, id=uuid)
    if document.peut_acceder(request.user):
        contenu_html = markdown(document.contenu, hard_wrap=True)
        return render(request, 'display_document.html',
                      {'document': document, 'contenu_html': contenu_html})
    raise PermissionDenied


@can_manage_wiki
def delete_document(request, uuid: int) -> HttpResponse:
    """ Supprime un document de la base de données

    :param request:
    :param uuid: id du document à supprimer
    :return:
    """
    document = get_object_or_404(Document, id=uuid)
    document.delete()
    return redirect('documents_tous')


@can_manage_wiki
def edit_document(request, uuid: int) -> HttpResponse:
    """ Modifie un document

    :param request:
    :param uuid: id du document à modifier
    :return:
    """
    document = get_object_or_404(Document, id=uuid)
    if request.method == 'POST':
        form = DocumentationForm(request.POST, instance=document)
        if form.is_valid():
            document = form.save()
            Modification(membre=request.user, document=document)
    else:
        form = DocumentationForm(instance=document)
    return render(request, 'edit_document.html',
                  {'form': form, 'uuid': uuid, 'document': document.titre})


@can_manage_wiki
def create_document(request) -> HttpResponse:
    """ Permet de créer un nouveau document

    :param request:
    :return:
    """
    if request.method == 'POST':
        form = DocumentationForm(request.POST)
        if form.is_valid():
            document = form.save()
            Modification(membre=request.user, document=document)
            return redirect('documents_tous')
    else:
        form = DocumentationForm()
    return render(request, 'create_document.html', {'form': form})


##########################################################################
#                                                                        #
# Gestion des images de document                                         #
#                                                                        #
##########################################################################

@can_manage_wiki
def upload_image(request) -> HttpResponse:
    """ Interface de chargement d'une image

    :param request:
    :return:
    """
    if request.method == "POST":
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            image = form.save()
            return JsonResponse({
                'success': True,
                'path': '![' + image.image.name + '](' + reverse("image_wiki",
                                                                 args=[
                                                                     image.id]) + ')'
            })
        return JsonResponse({'success': False})
    form = ImageUploadForm()
    html = render(request, 'upload_image.html', {'form': form})
    return html


@can_access_wiki
def get_image_wiki(request, image_id: int):
    """
    Permet aux utilisateurs connectés de récupérer les images du wiki

    :param request:
    :param image_id: id de l'image de wiki à télécharger
    :return:
    """
    image = get_object_or_404(Image, id=image_id)
    return media_response(image.image.url)


##########################################################################
#                                                                        #
# Téléchargement de documents                                            #
#                                                                        #
##########################################################################

@can_access_wiki
def download_document_pdf(request, uuid: int) -> HttpResponse:
    """ Permet de télécharger un document au format PDF, si l'utilisateur à le droit de le voir

    :param request:
    :param uuid: id du document à télécharger
    :return:
    """
    document = get_object_or_404(Document, id=uuid)
    if document.peut_acceder(request.user):
        html_text = markdown(document.contenu, hard_wrap=True)
        with TemporaryFile(mode='w+b') as f:
            pisa.CreatePDF(html_text.encode('utf-8'), dest=f, encoding='utf-8')
            f.seek(0)
            response = HttpResponse(f, content_type='application/pdf')
            response[
                'Content-Disposition'] = 'attachment; filename="' + document.nom_fichier + '.pdf' + '"'
            return response
    raise PermissionDenied


@can_access_wiki
def download_document_markdown(request, uuid: int) -> HttpResponse:
    """ Permet de télécharger un document au format Markdown, si l'utilisateur à le droit de le voir

    :param request:
    :param uuid: id du document à télécharger
    :return:
    """
    document = get_object_or_404(Document, id=uuid)
    if document.peut_acceder(request.user):
        with TemporaryFile(mode='w+', encoding='utf-8') as f:
            f.write(document.contenu)
            f.seek(0)
            response = HttpResponse(f, content_type='text/markdown')
            response[
                'Content-Disposition'] = 'attachment; filename="' + document.nom_fichier + '.md' + '"'
            return response
    raise PermissionDenied
