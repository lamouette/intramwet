from django.urls import path

from .views import get_image_wiki, manage_categories, delete_category, \
    modify_category, create_document, edit_document, delete_document, \
    download_document_pdf, download_document_markdown, upload_image, \
    display_document, display_all

urlpatterns = [
    path('categorie', manage_categories, name="gestion_categories"),
    path('categorie/suppression/<int:uuid>', delete_category,
         name="suppression_categorie"),
    path('categorie/modification/<int:uuid>', modify_category,
         name="modification_categorie"),
    path('document/ajout', create_document, name='ajout_document'),
    path('document/modification/<int:uuid>', edit_document,
         name='modification_document'),
    path('document/suppression/<int:uuid>', delete_document,
         name='suppression_document'),
    path('document/telechargement/pdf/<int:uuid>', download_document_pdf,
         name='telechargement_document_pdf'),
    path('document/telechargement/markdown/<int:uuid>',
         download_document_markdown,
         name='telechargement_document_markdown'),
    path('document/chargement/image', upload_image,
         name='chargement_image'),
    path('document/image/<int:image_id>', get_image_wiki, name="image_wiki"),
    path('document/<int:uuid>', display_document, name='document'),
    path('', display_all, name='documents_tous'),
]
