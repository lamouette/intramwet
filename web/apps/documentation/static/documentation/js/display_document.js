$(document).ready(function () {
    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
        onConfirm: function () {
            $(location).attr('href', urlSuppression);
        }
    });
});