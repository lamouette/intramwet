from django.contrib.auth.models import Group, Permission
from django.forms import ModelForm, MultipleChoiceField

from .models import Document, Categorie, Image


class ImageUploadForm(ModelForm):
    class Meta:
        model = Image
        fields = ['image']


class DocumentationForm(ModelForm):
    class Meta:
        model = Document
        fields = ['titre', 'categorie', 'contenu']


class CategorieForm(ModelForm):
    class Meta:
        model = Categorie
        fields = ['nom']

    def __init__(self, *args, **kwargs):
        super(CategorieForm, self).__init__(*args, **kwargs)

        choices = [(g.name, g.name) for g in Group.objects.all()]
        initial = []
        if self.instance:
            print(self.instance.full_permission_name)
            initial = [g.name for g in Group.objects.filter(
                permissions__codename=self.instance.permission_name)]
            print(initial)
        self.fields['groupesAyantAcces'] = MultipleChoiceField(
            label="Groupes ayant accès",
            choices=choices,
            initial=initial,
            required=False
        )

    def save(self, commit=True):
        m = super(CategorieForm, self).save(commit=False)
        if commit:
            m.save()

        permission = Permission.objects.get(
            codename=self.instance.permission_name)

        for group in Group.objects.filter(permissions=permission):
            group.permissions.remove(permission)

        for group_name in self.cleaned_data['groupesAyantAcces']:
            group = Group.objects.get(name=group_name)
            group.permissions.add(permission)

        return m
