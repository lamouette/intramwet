from datetime import datetime

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.utils.functional import cached_property


class Categorie(models.Model):
    nom = models.CharField("catégorie du document", max_length=50, unique=True)

    @property
    def permission_name(self):
        return "acceder_categorie_" + str(self.id)

    @cached_property
    def full_permission_name(self):
        return "documentation." + self.permission_name

    def create_permission(self):
        content_type = ContentType.objects.get_for_model(Categorie)
        Permission.objects.create(
            codename=self.permission_name,
            name='Peut accéder à la catégorie ' + str(self.id),
            content_type=content_type,
        )

    def get_permission(self):
        content_type = ContentType.objects.get_for_model(Categorie)
        return Permission.objects.get(codename=self.permission_name,
                                      content_type=content_type)

    def delete_permission(self):
        self.get_permission().delete()

    def __str__(self):
        return self.nom


@receiver(post_save, sender=Categorie)
def init_permission_on_category_created(sender, instance: Categorie,
                                        created: bool, **kwargs):
    """ Crée les permisssions associées à la nouvelle catégorie

    On utilise un signal afin que les catégories créées via l'interface admin aient aussi leurs permissions. La
    permission se crée après la sauvegarde de la nouvelle catégorie.
    :param sender:
    :param instance: la catégorie sauvegardée dans la BD
    :param created: si la catégorie vient d'être créée ou simplement modifiée
    :param kwargs:
    :return:
    """
    if created:
        instance.create_permission()


@receiver(pre_delete, sender=Categorie)
def delete_permission_on_category_deleted(sender, instance: Categorie,
                                          **kwargs):
    """ Supprime les permissions associées à la catégorie à supprimer

    On utilise un signal afin de supprimer les permissions associées aux catégories supprimées. Les permissions
    n'étant pas reliées à la Categorie par une clé étrangère (mais simplement par leur nom), on ne peut pas utiliser
    on_delete CASCADE pour les supprimer. Les permissions sont supprimées avant la suppression finale de la Categorie.
    :param sender:
    :param instance: la catégorie à supprimer
    :param kwargs:
    :return:
    """
    instance.delete_permission()


class Document(models.Model):
    titre = models.CharField("titre du document", max_length=50, unique=True)
    categorie = models.ForeignKey(Categorie, on_delete=models.CASCADE,
                                  null=True, blank=True)
    contenu = models.TextField()

    @cached_property
    def nom_fichier(self):
        label = self.titre
        label = label.replace(' ', '_').replace("'", '_').replace('é',
                                                                  'e').replace(
            'è', 'e').replace('ê', 'e') \
            .replace('à', 'a').replace('ù', 'u').replace('"', '_').replace('(',
                                                                           '_').replace(
            ')', '_') \
            .replace('[', '_').replace(']', '_').replace('{', '_').replace('}',
                                                                           '_').replace(
            '\\', '')
        label = ''.join(e for e in label if e.isalnum() or e == '_')
        return label

    def peut_acceder(self, utilisateur):
        if utilisateur.is_staff:
            return True

        if self.categorie:
            return utilisateur.has_perm(self.categorie.full_permission_name)

        return True

    def __str__(self):
        return self.categorie.nom + " | " + self.titre if self.categorie else self.titre


class Modification(models.Model):
    membre = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)
    document = models.ForeignKey(Document, on_delete=models.CASCADE)
    date = models.DateTimeField("date de modification", default=datetime.now)

    def __str__(self):
        return self.document.titre + " | " + str(
            self.date) + " | " + self.membre.nomComplet


class Image(models.Model):
    image = models.ImageField("Image du wiki", upload_to='images_wiki/')
