from datetime import datetime, timezone

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.templatetags.static import static
from django.utils.functional import cached_property


class TypeMateriel(models.Model):
    typeMateriel = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.typeMateriel


class ProprietaireManager(models.Manager):
    def emprunt_gratuit(self, utilisateur):
        return [p for p in self.all() if
                utilisateur.has_perm(p.emprunt_gratuit_full_permission_name)]

    def emprunt_payant(self, utilisateur):
        return [p for p in self.all() if
                utilisateur.has_perm(p.emprunt_payant_full_permission_name)]


class ProprietaireMateriel(models.Model):
    """
    Attention, si vous modifiez la structure des permissions, il faut absolument modifer le fichier
    materiel/migrations/init_db_2.py qui initialise les permissions et les attribue aux différents groupes
    """
    nomProprietaire = models.CharField("nom du propriétaire", max_length=50,
                                       unique=True)
    objects = ProprietaireManager()

    @property
    def emprunt_gratuit_permission_name(self):
        return "emprunt_gratiut_proprietaire_" + str(self.id)

    @property
    def emprunt_payant_permission_name(self):
        return "emprunt_payant_proprietaire_" + str(self.id)

    @cached_property
    def emprunt_gratuit_full_permission_name(self):
        return "materiel." + self.emprunt_gratuit_permission_name

    @cached_property
    def emprunt_payant_full_permission_name(self):
        return "materiel." + self.emprunt_payant_permission_name

    def create_emprunt_gratuit_permission(self):
        content_type = ContentType.objects.get_for_model(ProprietaireMateriel)
        Permission.objects.create(
            codename=self.emprunt_gratuit_permission_name,
            name='Peut emprunter gratuitement le matériel de la catégorie ' + str(
                self.id),
            content_type=content_type,
        )

    def create_emprunt_payant_permission(self):
        content_type = ContentType.objects.get_for_model(ProprietaireMateriel)
        Permission.objects.create(
            codename=self.emprunt_payant_permission_name,
            name='Peut louer le matériel de la catégorie ' + str(self.id),
            content_type=content_type,
        )

    def get_emprunt_gratuit_permission(self):
        content_type = ContentType.objects.get_for_model(ProprietaireMateriel)
        return Permission.objects.get(
            codename=self.emprunt_gratuit_permission_name,
            content_type=content_type,
        )

    def get_emprunt_payant_permission(self):
        content_type = ContentType.objects.get_for_model(ProprietaireMateriel)
        return Permission.objects.get(
            codename=self.emprunt_payant_permission_name,
            content_type=content_type,
        )

    def delete_emprunt_gratuit_permission(self):
        self.get_emprunt_gratuit_permission().delete()

    def delete_emprunt_payant_permission(self):
        self.get_emprunt_payant_permission().delete()

    def __str__(self):
        return self.nomProprietaire


@receiver(post_save, sender=ProprietaireMateriel)
def init_permissions_on_prorietaire_created(sender,
                                            instance: ProprietaireMateriel,
                                            created: bool, **kwargs):
    if created:
        instance.create_emprunt_gratuit_permission()
        instance.create_emprunt_payant_permission()


@receiver(pre_delete, sender=ProprietaireMateriel)
def delete_permissions_on_proprietaire_deleted(sender,
                                               instance: ProprietaireMateriel,
                                               **kwargs):
    instance.delete_emprunt_gratuit_permission()
    instance.delete_emprunt_payant_permission()


class MaterielEmpruntable:
    def __init__(self, materiel, emprunt_gratuit: bool):
        self.materiel = materiel
        self.emprunt_gratuit = emprunt_gratuit


class MaterielManager(models.Manager):
    def emprunt_gratuit(self, utilisateur):
        return self.filter(
            proprietaireMatos__in=ProprietaireMateriel.objects.emprunt_gratuit(
                utilisateur))

    def emprunt_payant(self, utilisateur):
        return self.filter(
            proprietaireMatos__in=ProprietaireMateriel.objects.emprunt_payant(
                utilisateur)).exclude(
            proprietaireMatos__in=ProprietaireMateriel.objects.emprunt_gratuit(
                utilisateur))

    def empruntable(self, utilisateur):
        return self.emprunt_gratuit(utilisateur) | self.emprunt_payant(
            utilisateur)

    def empruntable_categorise(self, utilisateur):
        materiel_gratuit = self.emprunt_gratuit(utilisateur)
        materiel_payant = self.emprunt_payant(utilisateur)
        materiel = {}
        for m in materiel_gratuit:
            if m.typeMateriel.typeMateriel not in materiel:
                materiel[m.typeMateriel.typeMateriel] = []
            materiel[m.typeMateriel.typeMateriel].append(
                MaterielEmpruntable(m, True))
        for m in materiel_payant:
            if m.typeMateriel.typeMateriel not in materiel:
                materiel[m.typeMateriel.typeMateriel] = []
            materiel[m.typeMateriel.typeMateriel].append(
                MaterielEmpruntable(m, False))
        return materiel

    def disponible(self, utilisateur: get_user_model(), date_debut: datetime,
                   date_fin: datetime) -> dict:
        """
        Permet de connaître le matos disponible, ainsi que sa quantité restante,
        en fonction de ce à quoi l'utilisateur peut accéder.

        :param utilisateur: l'utilisateur qui veut emprunter le matériel
        :param date_debut: date de début de l'emprunt
        :param date_fin: date de fin de l'emprunt
        :return: la liste du matériel empruntable au format JSON :
        {
            idMateriel1 : {'dispo' : True, 'quantite' : 2}
            idMateriel2 : {'dispo' : False}
            ...
        }
        """
        matos_emruntable = self.empruntable(utilisateur=utilisateur)
        matos_disponible = {}
        for m in matos_emruntable:
            quantite_restante = m.quantite_restante(date_debut=date_debut,
                                                    date_fin=date_fin)
            matos_disponible[m.id] = {
                'dispo': quantite_restante > 0,
                'quantite': quantite_restante
            }
        return matos_disponible


class Materiel(models.Model):
    nom = models.CharField(max_length=100)
    typeMateriel = models.ForeignKey(TypeMateriel, on_delete=models.CASCADE)
    proprietaireMatos = models.ForeignKey(ProprietaireMateriel,
                                          on_delete=models.CASCADE)
    anneeAchat = models.DateField(null=True)
    montantCaution = models.FloatField(null=True)
    prixLocation = models.FloatField("Prix de location du matériel", default=0)
    photo = models.ImageField(upload_to='photos_matos/%Y/', null=True)
    description = models.TextField(null=True)
    quantite = models.IntegerField(default=1)
    objects = MaterielManager()

    def __str__(self):
        return "%s | %s | %d" % (self.typeMateriel, self.nom, self.quantite)

    def quantite_restante(self, date_debut: datetime, date_fin: datetime,
                          strict: bool = True) -> int:
        elements = EmpruntElement.objects.intersecting(id_materiel=self.id,
                                                       date_debut=date_debut,
                                                       date_fin=date_fin,
                                                       strict=strict)
        return self.quantite - sum(e.quantite for e in elements)

    @cached_property
    def photo_url(self):
        return self.photo.url if self.photo else static('images/pas_de_photo.png')


class EmpruntManager(models.Manager):
    def intersecting(self, date_debut: datetime, date_fin: datetime):
        return self.filter(debut__lt=date_fin, fin__gt=date_debut)


class Emprunt(models.Model):
    membre = models.ForeignKey(get_user_model(), on_delete=models.PROTECT,
                               related_name='membre')
    debut = models.DateTimeField()
    fin = models.DateTimeField()
    description = models.TextField(null=True)
    commentaire = models.TextField(null=True, blank=True)
    dateCommentaire = models.DateTimeField(null=True, blank=True)
    auteurCommentaire = models.ForeignKey(get_user_model(), null=True, blank=True,
                                          on_delete=models.PROTECT,
                                          related_name='auteurCommentaire')
    objects = EmpruntManager()

    def __str__(self):
        return "%s | %s | %s" % (self.membre, self.debut, self.fin)

    @cached_property
    def nb_jours_emprunt(self):
        return max(1, (self.fin.date() - self.debut.date()).days)


class EmpruntMateriel(Emprunt):
    def est_en_attente(self):
        if self.elements.all().filter(statut=EmpruntElement.EN_ATTENTE):
            return True
        else:
            return False

    def est_a_venir(self):
        if not self.est_termine() and not self.est_en_attente():
            if self.fin > datetime.now(timezone.utc):
                return True
            else:
                return False
        else:
            return False

    def est_en_retard(self):
        if not self.est_termine() and not self.est_en_attente():
            if self.fin <= datetime.now(timezone.utc):
                return True
            else:
                return False
        else:
            return False

    def est_termine(self):
        if not self.elements.all().exclude(
                statut=EmpruntElement.REFUSE).exclude(
            statut=EmpruntElement.RENDU):
            return True
        else:
            return False

    def elements_payants(self):
        materiel_payant_utilisateur = Materiel.objects.emprunt_payant(
            self.membre)
        return self.elements.filter(materiel__in=materiel_payant_utilisateur)


class EmpruntLocal(Emprunt):
    VALIDE = "VAL"
    REFUSE = "REF"
    EN_ATTENTE = "ATT"
    statut = models.CharField(
        max_length=3,
        choices=[
            (VALIDE, "Validé"),
            (REFUSE, "Refusé"),
            (EN_ATTENTE, "En attente"),
        ],
        default=EN_ATTENTE,
    )
    empruntGdM = models.BooleanField("Est un emprunt de GdM ?", default=False)

    @classmethod
    def local_est_emprunte(cls, date_debut: datetime, date_fin: datetime,
                           strict: bool = True) -> bool:
        emprunts = cls.objects.intersecting(date_debut, date_fin)
        if strict:
            return emprunts.filter(statut=cls.VALIDE).exists()

        return emprunts.exclude(statut=cls.REFUSE).exists()


class EmpruntElementManager(models.Manager):
    def intersecting(self, id_materiel: int, date_debut: datetime,
                     date_fin: datetime, strict: bool = True):
        """
            Renvoie les EmpruntElements intersectant avec la période sélectionnée
        """

        emprunts_intersectant = EmpruntMateriel.objects.intersecting(
            date_debut=date_debut, date_fin=date_fin)
        emprunts_elements = EmpruntElement.objects.filter(
            materiel__id=id_materiel, emprunt__in=emprunts_intersectant)
        return emprunts_elements.filter(
            statut=EmpruntElement.VALIDE) if strict else emprunts_elements.exclude(
            statut=EmpruntElement.REFUSE)


class EmpruntElement(models.Model):
    emprunt = models.ForeignKey(EmpruntMateriel, on_delete=models.CASCADE,
                                related_name='elements')
    materiel = models.ForeignKey(Materiel, on_delete=models.PROTECT)

    VALIDE = "VAL"
    REFUSE = "REF"
    EN_ATTENTE = "ATT"
    RENDU = "REN"
    statut = models.CharField(
        max_length=20,
        choices=[
            (VALIDE, "Validé"),
            (REFUSE, "Refusé"),
            (EN_ATTENTE, "En attente"),
            (RENDU, "Rendu"),
        ],
        default=EN_ATTENTE,
    )

    quantite = models.IntegerField(default=1)
    commentaire = models.TextField(blank=True)
    validePar = models.ForeignKey(get_user_model(), null=True, blank=True,
                                  on_delete=models.PROTECT)
    objects = EmpruntElementManager()

    def __str__(self):
        return "%s | %s | %s" % (self.emprunt, self.materiel, self.statut)
