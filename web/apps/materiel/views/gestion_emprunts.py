from datetime import datetime

from django.http import JsonResponse
from django.shortcuts import render, redirect

from ..decorators import can_manage_materiel
from ..models import EmpruntElement, EmpruntMateriel, Materiel, Emprunt
from apps.conf.bot import Email
from apps.passation.decorators import can_access_passation, register


@can_manage_materiel
def emprunt_gestion(request):
    """
    Génère la page de gestion des emprunts.
    """

    statut_valide = EmpruntElement.VALIDE
    statut_refuse = EmpruntElement.REFUSE
    statut_en_attente = EmpruntElement.EN_ATTENTE
    statut_rendu = EmpruntElement.RENDU

    emprunts_a_venir = EmpruntMateriel.objects.none()
    emprunts_retards = EmpruntMateriel.objects.none()
    emprunts_en_attente = EmpruntMateriel.objects.none()
    emprunts_termines = EmpruntMateriel.objects.none()

    for emp in EmpruntMateriel.objects.all():
        if emp.est_termine():
            emprunts_termines = emprunts_termines.union(
                EmpruntMateriel.objects.filter(id=emp.id))
        elif emp.est_a_venir():
            emprunts_a_venir = emprunts_a_venir.union(
                EmpruntMateriel.objects.filter(id=emp.id))
        elif emp.est_en_attente():
            emprunts_en_attente = emprunts_en_attente.union(
                EmpruntMateriel.objects.filter(id=emp.id))
        elif emp.est_en_retard():
            emprunts_retards = emprunts_retards.union(
                EmpruntMateriel.objects.filter(id=emp.id))
        else:
            print("Erreur : Emprunt non valide ({}) !".format(emp))

    emprunts_a_venir = emprunts_a_venir.order_by('-debut')
    emprunts_retards = emprunts_retards.order_by('-debut')
    emprunts_en_attente = emprunts_en_attente.order_by('-debut')
    emprunts_termines = emprunts_termines.order_by('-debut')

    matos_emprunt_gere = EmpruntElement.objects.filter(
        emprunt__in=emprunts_a_venir.values('id'))
    matos_emprunt_en_attente = EmpruntElement.objects.filter(
        emprunt__in=emprunts_en_attente.values('id'))
    matos_emprunt_retard = EmpruntElement.objects.filter(
        emprunt__in=emprunts_retards.values('id'))
    matos_emprunt_termine = EmpruntElement.objects.filter(
        emprunt__in=emprunts_termines.values('id'))

    # Emprunts éléments ayant des conflits avec n'importe quel élément
    matos_emprunt_conflit = EmpruntElement.objects.none()
    for elem in EmpruntElement.objects.exclude(
            emprunt__in=EmpruntMateriel.objects.exclude(
                elements__statut=statut_en_attente)):
        if elem.materiel.quantite_restante(date_debut=elem.emprunt.debut,
                                           date_fin=elem.emprunt.fin,
                                           strict=False) < 0:
            matos_emprunt_conflit = matos_emprunt_conflit.union(
                EmpruntElement.objects.filter(id=elem.id))

    # Emprunts éléments ayant des conflits avec des éléments déjà validés
    matos_emprunt_conflit_gere = EmpruntElement.objects.none()
    for elem in matos_emprunt_conflit:
        if EmpruntElement.objects.filter(
                id__in=EmpruntElement.objects.intersecting(
                    id_materiel=elem.materiel.id,
                    date_debut=elem.emprunt.debut,
                    date_fin=elem.emprunt.fin,
                    strict=False).values_list('id', flat=True)).exclude(
            statut=statut_en_attente):
            matos_emprunt_conflit_gere = matos_emprunt_conflit_gere.union(
                EmpruntElement.objects.filter(id=elem.id))

    # Emprunts ayant des conflits
    emprunts_conflits = EmpruntMateriel.objects.filter(
        id__in=matos_emprunt_conflit.values_list('emprunt__id', flat=True))

    return render(request, "emprunt_gestion.html", {
        'emprunts_en_attente': emprunts_en_attente,
        'emprunts_a_venir': emprunts_a_venir,
        'matos_emp_gere': matos_emprunt_gere,
        'emprunts_termines': emprunts_termines,
        'matos_emp_en_attente': matos_emprunt_en_attente,
        'statut_valide': statut_valide, 'statut_rendu': statut_rendu,
        'statut_refuse': statut_refuse,
        'matos_emprunt_retard': matos_emprunt_retard,
        'statut_en_attente': statut_en_attente,
        'matos_emp_conflit': matos_emprunt_conflit,
        'emprunts_conflits': emprunts_conflits,
        'matos_emp_conflit_gere': matos_emprunt_conflit_gere,
        'emprunts_retards': emprunts_retards,
        'matos_emprunt_termine': matos_emprunt_termine,
    })


@can_manage_materiel
def emprunt_statut(request):
    """
    Met à jour la valeur du statut d'un EmpruntElement si l'élèment est en attente.

    :return: Données JSON
        {'nb_en_attente': <Nombre d'EmpruntElement encore en attente sur cet Emprunt>}
    """

    data = {}
    if request.method == 'POST':
        if 'id' in request.POST and 'valider' in request.POST:
            element = EmpruntElement.objects.get(id=request.POST.get('id'))
            if element.statut == EmpruntElement.EN_ATTENTE:
                element.statut = EmpruntElement.VALIDE if (
                        int(request.POST.get(
                            'valider')) == 1) else EmpruntElement.REFUSE
            elif element.statut == EmpruntElement.VALIDE and int(
                    request.POST.get('valider')) == 2:
                element.statut = EmpruntElement.RENDU

            if element.emprunt.est_en_attente():
                en_att = True
                if 'commentaire' in request.POST:
                    if request.POST.get('commentaire'):
                        element.commentaire = request.POST.get('commentaire')
                        element.date = datetime.now()
                        element.validePar = request.user
                if 'commentaireEmprunt' in request.POST:
                    element.emprunt.commentaire = request.POST.get(
                        'commentaireEmprunt')
                    element.emprunt.dateCommentaire = datetime.now()
                    element.emprunt.auteurCommentaire = request.user
            else:
                en_att = False
            element.save()

            if en_att:
                nb_en_attente = EmpruntMateriel.objects.filter(
                    elements__statut=EmpruntElement.EN_ATTENTE,
                    id=element.emprunt.id).count()
            else:
                nb_en_attente = EmpruntMateriel.objects.filter(
                    elements__statut=EmpruntElement.VALIDE,
                    id=element.emprunt.id).count()
            data['nb_en_attente'] = nb_en_attente

            if en_att and nb_en_attente == 0:
                emprunt = element.emprunt
                user = emprunt.membre
                est_refuse = not EmpruntElement.objects.filter(emprunt=emprunt,
                                                               statut=EmpruntElement.VALIDE).exists()
                VALIDE, REFUSE = (EmpruntElement.VALIDE, EmpruntElement.REFUSE)
                Email('emails/emprunt_valide.html', user.email,
                      locals()).send()

    return JsonResponse(data)


@can_manage_materiel
def ajout_emprunt_element(request):
    """
    Ajoute un EmpruntElement à l'emprunt passé en paramètre
    """

    if request.method == 'POST':
        if 'id_emprunt' in request.POST and 'id_matos' in request.POST:
            matos = Materiel.objects.get(id=int(request.POST.get('id_matos')))
            emprunt = EmpruntMateriel.objects.get(
                id=int(request.POST.get('id_emprunt')))
            if matos.quantite_restante(date_debut=emprunt.debut,
                                       date_fin=emprunt.fin) > 0:
                # Si l'emprunt possède déjà un élément du matériel demandé, on
                # y rajoute une quantité
                if emprunt.elements.all().filter(materiel__id=matos.id,
                                                 statut=EmpruntElement.VALIDE):
                    element = emprunt.elements.all().get(materiel_id=matos.id)
                    element.quantite += 1
                    element.save()
                else:  # Sinon on crée l'EmpruntElément correspondant
                    element = EmpruntElement(emprunt=emprunt, materiel=matos,
                                             quantite=1,
                                             statut=EmpruntElement.EN_ATTENTE)
                    element.save()

    return JsonResponse({})


@register(url='emprunts/supprimer', name='supprimer_emprunts',
          verbose_name="Supprimer tous les emprunts",
          fa_icon="fas fa-trash", btn_class="btn-danger delete")
@can_access_passation
def supprimer_emprunts(request):
    """ Supprime toutes les notes de frais

    :param request:
    :return:
    """
    for emprunt in Emprunt.objects.all():
        emprunt.delete()
    return redirect('passation')

