from datetime import datetime

from django.http import JsonResponse
from django.shortcuts import render
from django.utils import timezone

from ..decorators import can_manage_local
from ..models import EmpruntLocal
from apps.conf.bot import Email


@can_manage_local
def local_gestion(request):
    emprunts_en_attente = EmpruntLocal.objects.filter(
        statut=EmpruntLocal.EN_ATTENTE).order_by('debut')
    emprunts_a_venir = EmpruntLocal.objects.filter(statut=EmpruntLocal.VALIDE,
                                                   fin__gte=datetime.now(
                                                       timezone.utc)).order_by(
        'debut')
    emprunts_termines = EmpruntLocal.objects.filter(
        fin__lt=datetime.now(timezone.utc)) \
        .union(EmpruntLocal.objects.filter(
        statut=EmpruntLocal.REFUSE)).distinct().order_by('-debut')
    valide = EmpruntLocal.VALIDE
    refuse = EmpruntLocal.REFUSE

    return render(request, 'local_gestion.html', locals())


@can_manage_local
def local_statut(request):
    if request.method == 'POST':
        valider = int(request.POST['valider'])
        emprunt = EmpruntLocal.objects.get(id=request.POST['id_emprunt'])
        commentaire = request.POST['commentaire']

        if valider:
            if not EmpruntLocal.local_est_emprunte(emprunt.debut, emprunt.fin):
                emprunt.statut = EmpruntLocal.VALIDE
                Email('emails/emprunt_local_valide.html', emprunt.membre.email,
                      {'emprunt': emprunt, 'valide': True}).send()
        else:
            emprunt.statut = EmpruntLocal.REFUSE
            Email('emails/emprunt_local_valide.html', emprunt.membre.email,
                  {'emprunt': emprunt, 'valide': False}).send()
        emprunt.commentaire = commentaire
        emprunt.auteurCommentaire = request.user
        emprunt.dateCommentaire = datetime.now(timezone.utc)

        emprunt.save()
    return JsonResponse({})