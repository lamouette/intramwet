from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from ..decorators import can_manage_materiel
from ..forms import AjoutMateriel
from ..models import EmpruntMateriel, Materiel, TypeMateriel


@can_manage_materiel
def gestion_materiel(request):
    """
    Génère la page de gestion du matériel, ou ajoute un nouveau matériel à la base.
    """

    materiels = Materiel.objects.all().order_by('typeMateriel__typeMateriel',
                                                'nom')
    form = AjoutMateriel()
    alerts = []
    if request.method == 'POST':
        form = AjoutMateriel(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            alerts = [{
                'type': 'success',
                'titre': 'Bravo : ',
                'contenu': 'Vous avez correctement enregistré votre matériel !'
            }]
            form = AjoutMateriel()
        else:
            alerts = [{
                'type': 'danger',
                'titre': 'Formulaire non valide : ',
                'contenu': 'Merci de spécifier correctement le contenu de chaque champ.'
            }]
    return render(request, "gestion_materiel.html",
                  {'materiels': materiels, 'form': form, 'alerts': alerts})


@can_manage_materiel
def search_materiel(request):
    """
    Renvoie des résultats d'une recherche sur la base de données de matériel.
    """

    data = {}
    if request.method == 'POST':
        if 'emprunt' not in request.POST and 'texte' not in request.POST:
            return JsonResponse(data)

        emprunt = EmpruntMateriel.objects.get(
            id=int(request.POST.get('emprunt')))
        data = {'results': []}
        for matos in Materiel.objects.filter(
                nom__icontains=request.POST.get('texte')):
            if matos.quantite_restante(date_debut=emprunt.debut,
                                       date_fin=emprunt.fin) > 0:
                data['results'].append([matos.id, matos.nom])

    return JsonResponse(data)


@can_manage_materiel
def suppr_materiel(request):
    if request.method == 'POST':
        try:
            Materiel.objects.get(id=int(request.POST.get('id'))).delete()
            return JsonResponse(
                {'success': True, 'id': int(request.POST.get('id'))})
        except ValueError:
            return JsonResponse({'success': False})

    return JsonResponse({'success': False})


@can_manage_materiel
def ajout_type_materiel_modal(request):
    return render(request, 'ajout_type_materiel_modal.html')


@can_manage_materiel
def modifier_materiel_modal(request):
    if request.method != 'POST':
        return HttpResponse('Erreur de requête HTTP !')

    if 'id' not in request.POST:
        return HttpResponse('Erreur de requête HTTP !')

    form = AjoutMateriel(
        instance=Materiel.objects.get(id=request.POST.get('id')))
    return render(request, 'modifier_materiel_modal.html',
                  {'form': form, 'id': request.POST.get('id')})


@can_manage_materiel
def ajout_type_materiel(request):
    if request.method == 'POST':
        nom = request.POST.get('type')
        TypeMateriel(typeMateriel=nom).save()
        return JsonResponse({'success': True})
    else:
        return JsonResponse({'success': False})


@csrf_exempt
@can_manage_materiel
def modifier_materiel(request):
    if request.method == 'POST':
        if 'id' in request.POST:
            matos = Materiel.objects.get(id=request.POST.get('id'))
            form = AjoutMateriel(request.POST, request.FILES, instance=matos)
            if form.is_valid():
                form.save()
                return JsonResponse({'success': True})
    return JsonResponse({'success': False})