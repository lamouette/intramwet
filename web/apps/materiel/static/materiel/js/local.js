let form;
$(function () {
    let picker_debut = $('#id_debut');
    let picker_fin = $('#id_fin');

    let date_debut = (picker_debut.val() === '') ? moment().add(3, 'days').hours(13).minutes(0) : moment(picker_debut.val(), 'DD/MM/YYYY HH:mm');
    let date_fin = (picker_fin.val() === '') ? moment().add(4, 'days').hours(13).minutes(0) : moment(picker_fin.val(), 'DD/MM/YYYY HH:mm');

    picker_debut.datetimepicker({
        locale: 'fr',
        format: 'DD/MM/YYYY HH:mm',
        date: null
    });
    picker_debut.datetimepicker('date', date_debut);
    picker_fin.datetimepicker({
        locale: 'fr',
        format: 'DD/MM/YYYY HH:mm',
        date: null
    });
    picker_fin.datetimepicker('date', date_fin);

    $('#id_debut, #id_fin').on('change.datetimepicker', function (e) {
        let is_valid = true;
        if (!moment($(this).val(), 'DD/MM/YYYY HH:mm').isValid()) {
            $(this).addClass('is-invalid');
            is_valid = false;
        } else {
            $(this).removeClass('is-invalid');
            let debut = moment($('#id_debut').val(), 'DD/MM/YYYY HH:mm');
            let fin = moment($('#id_fin').val(), 'DD/MM/YYYY HH:mm');

            if (debut.isSame(fin)) {
                $('#alerte-dates-egales').show();
                is_valid = false;
            } else {
                $('#alerte-dates-egales').hide();
            }

            if (debut.isAfter(fin)) {
                $('#alerte-dates-inversees').show();
                is_valid = false;
            } else {
                $('#alerte-dates-inversees').hide();
            }
        }

        if (is_valid)
            check_dispo();
    });

    form = $('#demande-emprunt');
    form.validate({
        rules: {
            raison: {
                required: false
            }
        }
    });
    check_dispo();
});

function check_dispo() {
    bloc = $('#disponibilite_local');
    bloc.parent().parent().parent().css('display', 'block');
    bloc.html('<i class="fas fa-spinner fa-spin fa-lg"></i> Vérification de la disponibilité ...');
    if (form.valid()) {
        $.ajax({
            url: url_local_disponibilite,
            type: 'post',
            data: $(form).serialize(),
            success: function (data) {
                bloc = $('#disponibilite_local');
                if ($.isEmptyObject(data)) {
                    $('.date input').addClass('is-invalid');
                } else if (data['disponible']) {
                    bloc.parent().parent().parent().css('display', 'block');
                    bloc.html('Le local est disponible !');
                } else {
                    bloc.parent().parent().parent().css('display', 'block');
                    bloc.html('Le local n\'est pas disponible sur ces dates !');
                }
            }
        });
    } else {
        $('.date input').addClass('is-invalid');
    }
}