from django.apps import AppConfig


class MaterielConfig(AppConfig):
    name = 'apps.materiel'
