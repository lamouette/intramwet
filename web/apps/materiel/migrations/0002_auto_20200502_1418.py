# Generated by Django 3.0.5 on 2020-05-02 12:18

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('materiel', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='empruntelement',
            name='validePar',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='emprunt',
            name='auteurCommentaire',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='auteurCommentaire', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='emprunt',
            name='membre',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='membre', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='empruntelement',
            name='emprunt',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='elements', to='materiel.EmpruntMateriel'),
        ),
    ]
