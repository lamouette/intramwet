## ARTICLE 1 : Objet

Les présentes « conditions générales d'utilisation » ont pour objet l'encadrement juridique de l’utilisation du site {{INTRAMWET_NAME}} et de ses services.

Ce contrat est conclu entre :

- Le gérant du site internet, ci-après désigné « l’Éditeur »
- Toute personne physique ou morale souhaitant accéder au site et à ses services, ci-après appelé « l’Utilisateur ».

Les conditions générales d'utilisation doivent être acceptées par tout Utilisateur, et son accès au site vaut acceptation de ces conditions.

## ARTICLE 2 : Mentions légales

### 2.1. Éditeur

Le site {{INTRAMWET_NAME}} est édité par:

**{{NAME}} – {{LEGAL_STATUS}}**
{{ADDRESS}}
{{ADDRESS_POSTAL_CODE}} {{ADDRESS_CITY}} {% if ADDRESS_POSTAL_CODE_CEDEX %}CEDEX{% endif %} {{ADDRESS_COUNTRY}}
[{{CONTACT_EMAIL}}](mailto:{{CONTACT_EMAIL}})

### 2.2. Hébergeur

Le site {{INTRAMWET_NAME}} est hébergé par {{INTRAMWET_HOSTING_COMPANY_NAME}} en {{INTRAMWET_HOSTING_LOCATION}}.

{{INTRAMWET_HOSTING_COMPANY_ADDRESS}}

## ARTICLE 3 : Accès aux services

### 3.1. Prérequis matériels

L'accès au services proposés par {{INTRAMWET_NAME}} n'est possible que si l'Utilisateur bénéficie d'une configuration informatique compatible, qui respecte les prérequis suivants :

- le matériel de l'Utilisateur est récent et ne contient pas de virus ;
- le matériel de l'Utilisateur dispose d'un navigateur de dernière génération mis à jour ; 
- le matériel de l'Utilisateur prend en charge la technologie JavaScript ;
- plus généralement, l'Utilisateur dispose des équipements et moyens matériels lui permettant de bénéficier des services.

Le site {{INTRAMWET_NAME}} ne prend pas en charge les navigateurs Internet Explorer.

### 3.2. Conditions d'accès

L'accès à la plateforme est ouvert à toute personne se connectant à partir de l'adresse URL <{{INTRAMWET_URL}}>.

Certaines fonctionnalités de la plateforme ne sont disponibles qu'après inscription. D'autres sont restreintes aux membres de l'association {{NAME}}.

### 3.3. Niveaux de disponibilité

Le site {{INTRAMWET_NAME}} est accessible à tout moment, sauf cas de force majeure et sous réserve des pannnes et des interventions de maintenance. 

{{NAME}} pourra interrompre l'accès au site {{INTRAMWET_NAME}}, pour des raisons de maintenance, sans aucun avertissement préalable.

### 3.4. Description des services 

Le site {{INTRAMWET_NAME}} a pour objet de fournir des informations concernant l'ensemble des activités de l'association. {{NAME}} ne pourra être tenue responsable des inexactitudes, des oublis ou des carences dans la mise à jour de ces informations. Toutes les informations indiquées sur le site sont données à titre indicatif, et sont susceptibles d’évoluer.

## ARTICLE 4 : Responsabilité de l'Utilisateur

L'Utilisateur est responsable des risques liés à l’utilisation de son identifiant de connexion et de son mot de passe. 

Le mot de passe de l’Utilisateur doit rester secret. En cas de divulgation de mot de passe, l’Éditeur décline toute responsabilité.

L’Utilisateur assume l’entière responsabilité de l’utilisation qu’il fait des informations et contenus présents sur le site {{INTRAMWET_NAME}}.

Tout usage du service par l'Utilisateur ayant directement ou indirectement pour conséquence des dommages doit faire l'objet d'une indemnisation au profit du site.

Le site permet aux membres de publier sur le site :
- Commentaires ;
- Photos ;

Le membre s’engage à tenir des propos respectueux des autres et de la loi et accepte que ces publications soient modérées ou refusées par l’Éditeur, sans obligation de justification.  Le cas échéant, {{NAME}} se réserve également la possibilité de mettre en cause la responsabilité civile et/ou pénale de l’Utilisateur, notamment en cas de message à caractère raciste, injurieux, diffamant, ou pornographique, quel que soit le support utilisé (texte, photographie …). 

En publiant sur le site, l’Utilisateur cède à {{NAME}} le droit non exclusif et gratuit de représenter, reproduire, adapter, modifier, diffuser et distribuer sa publication, directement ou par un tiers autorisé.

L’Éditeur s'engage toutefois à citer le membre en cas d’utilisation de  sa publication

## ARTICLE 5 : Responsabilité de l'Éditeur

Tout dysfonctionnement du serveur ou du réseau ne peut engager la responsabilité de l’Éditeur.

De même, la responsabilité du site ne peut être engagée en cas de force majeure ou du fait imprévisible et insurmontable d'un tiers.

Le site {{INTRAMWET_NAME}} s'engage à mettre en œuvre tous les moyens nécessaires pour garantir la sécurité et la confidentialité des données. Toutefois, il n’apporte pas une garantie de sécurité totale.

L’Éditeur se réserve la faculté d’une non-garantie de la fiabilité des sources, bien que les informations diffusées sur le site soient réputées fiables.

## ARTICLE 6 : Propriété intellectuelle

Les contenus du site {{INTRAMWET_NAME}} (logos, textes, éléments graphiques, vidéos, etc) sont protégés par le droit d'auteur, en vertu du Code de la propriété intellectuelle.

L’Utilisateur devra obtenir l’autorisation de l’Éditeur avant toute reproduction, copie ou publication de ces différents contenus.

Ces derniers peuvent être utilisés par les utilisateurs à des fins privées ; tout usage commercial est interdit.

L’Utilisateur est entièrement responsable de tout contenu qu’il met en ligne et il s’engage à ne pas porter atteinte à un tiers.

L’Éditeur du site se réserve le droit de modérer ou de supprimer librement et à tout moment les contenus mis en ligne par les utilisateurs, et ce sans justification.

## ARTICLE 7 : Données personnelles

Sont considérées comme « données personnelles » les informations « qui permettent, sous quelque forme que ce soit, directement ou non, l'identification des personnes physiques auxquelles elles s'appliquent » (article 4 de la loi n° 78-17 du 6 janvier 1978).

Les termes « données à caractère personnel », « personne concernée », « sous traitant » et « données sensibles » ont le sens défini par le Règlement Général sur la Protection des Données (RGPD : n° 2016-679).

{{NAME}} garantie le respect de la vie privée de l'Utilisateur, conformément à la Loi n° 78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et au Règlement Général sur la Protection des Données (RGPD : n° 2016-679).

### 7.1. Responsable du traitement

Le responsable du traitement des Données Personnelles est : __{{NAME}} – {{LEGAL_STATUS}}__. {{NAME}} est représentée par son Président, qui est son représentant légal.

### 7.2. Types de données collectées et finalités

Pour permettre la navigation sur le Site:
- données de connexion (dont le mot de passe, hashé) ;
- données d'utilisation.

Pour permettre la gestion et la traçabilité des prestations de {{NAME}} :
- coordonnées personnelles (nom, prénom, date de naissance et adresse postale).

Pour prévenir et lutter contre la fraude informatique et améliorer les services offerts par le Site :
- matériel informatique utilisé pour la navigation ;
- l’adresse IP.

Pour la communication d'informations diverses liées à la vie de l'association :
- numéro de téléphone ;
- adresse email.

### 7.3. Durée de conservation des données

Sauf demande explicite de la part de l'Utilisateur, les données collectées seront conservées pour une période de __36 mois__ après la fin de la relation contractuelle.

### 7.4. Destinataires des données

Les informations recueillies sur {{INTRAMWET_NAME}} sont accessibles par le conseil d'administration de {{NAME}}, l'équipe technique de {{INTRAMWET_NAME}} ainsi que les autres membres de l'association {{NAME}}.

Aucune information personnelle de l'utilisateur du site {{INTRAMWET_NAME}} n'est publiée à l'insu de l'utilisateur, échangée, transférée, cédée ou vendue sur un support quelconque à des tiers.

### 7.5. Caractère obligatoire du recueil des données

Sauf mention du contraire, le recueil des données personnelles est obligatoire pour le traitement de la demande d'inscription sur le site {{INTRAMWET_NAME}}.

### 7.6. Droit de l'Utilisateur

Conformément à la Loi n° 78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et au Règlement Général sur la Protection des Données (RGPD : n° 2016-679), l'Utilisateur dispose des droits suivants:

- droit d'information : l'Utilisateur a le droit de demander à tout moment d'être informé sur els traitements dont ses données personnelles font l'objet, ainsi que de pouvoir être informé de la finalité et des modalités du traitement ;
- droit d'accès : l’Utilisateur a le droit d’accéder aux données personnelles le concernant ;
- droit de rectification : l'Utilisateur a le droit d’obtenir du responsable du traitement, dans les meilleurs délais, la rectification des données à caractère personnel la concernant qui sont inexactes; l'Utilisateur a le droit d’obtenir que les données à caractère personnel incomplètes soient complétées, y compris en fournissant une déclaration complémentaire ;
- droit d'opposition : l'Utilisateur a le droit de s'opposer à tout moment, pour des raisons tenant à sa situation particulière, à un traitement des données à caractère personnel ;
- droit à l'effacement : l'Utilisateur a le droit de demander que ses données personnelles soient effacées pour les raisons suivantes:
    - les données collectées ne sont plus nécessaires au regard des finalités pour lesquelles elles ont été collectées ;
    - l’utilisateur retire son consentement (applicable que si le consentement était la seule base légale au traitement) ;
    - la personne s’oppose au traitement – hors des cas de consentement – et le responsable du traitement n’oppose pas de motif légitime et impérieux justifiant que le traitement « prévaut » sur les libertés individuelles de la personne concernée ;
    - les données ont fait ou font l’objet d’un traitement illicite, ou doivent être effacées en vertu d’une disposition légale ou d’une exigence judiciaire ;
    - les données d’un mineur de moins de 16 ans ont été recueillies sans autorisation parentale3.
- droit à la limitation du traitement : l'Utilisateur a le droit de demander que le traitement de ses données soit restreint au strict minimum (par exemple si la donnée en question est difficilement effaçable instantanément) ;
- droit à la portabilité : l'Utilisateur a le droit d’obtenir toutes les données qu’il a communiquées à un service, dans un format ouvert et interopérable, dans l’objectif de fournir ce fichier à un autre service qui gérera ses données
- droit de définir le sort des données post-mortem : l'Utilisateur a le droit de  définir des directives relatives à la conservation, à l’effacement et à la communication de ses données à caractère personnel après son décès.

### 7.7. Droit d'accès et de modification, droit à l'oubli

Pour modifier certaines informations personnelles (adresse électronique, adresse postale, numéro de téléphone et photo de profil) ainsi que son mot de passe, l'Utilisateur peut utiliser la page de modification du profil disponible via l'onglet « Membre » - « Mon profil ».

Pour demander une modification de ses données personnelles, pour exercer son droit à l'oubli ou pour toute autre question relative au traitement de ses données personnelles, l'Utilisateur peut utiliser l'adresse électronique [{{SUPPORT_EMAIL}}](mailto:{{SUPPORT_EMAIL}}) ou l'adresse postale suivante:

**{{NAME}}**
{{ADDRESS}}
{{ADDRESS_POSTAL_CODE}} {{ADDRESS_CITY}} {% if ADDRESS_POSTAL_CODE_CEDEX %}CEDEX{% endif %} {{ADDRESS_COUNTRY}}

### 7.8. Sécurité

Pour assurer la sécurité et la confidentialité des Données Personnelles des utilisateurs, le site {{INTRAMWET_NAME}} utilise des dispositifs standards tels que les pare-feux, le chiffrement TLS et le hashage du mot de passe.

Lors du traitement des Données Personnelles, {{NAME}} prend toutes les mesures raisonnables visant à les protéger contre toute perte, utilisation détournée, accès non autorisé, divulgation, altération ou destruction.

### 7.9. Notification d’incident

Malgré nos efforts, nous ne pouvons pas garantir une sécurité absolue de vos données. Si nous prenions connaissance d'une brèche de la sécurité, nous nous engageons à avertir les Utilisateurs concernés afin qu'ils puissent prendre les mesures appropriées.
 
## ARTICLE 8 : Liens hypertextes

Les domaines vers lesquels mènent les liens hypertextes présents sur le site n’engagent pas la responsabilité de l’Éditeur, qui n’a pas de contrôle sur ces liens.

Il est possible pour un tiers de créer un lien vers une page du site médialamouette.fr sans autorisation expresse de l’Éditeur.

## ARTICLE 9 : Cookies

Sauf si vous décidez de désactiver les cookies, vous acceptez que le site puisse les utiliser. Vous pouvez à tout moment désactiver les cookies à partir des possibilités de désactivation qui vous sont offertes et rappelées ci-après, sachant que cela peut réduire ou empêcher l’accessibilité à tout ou partie des Services proposés par le site.

### 9.1. Qu'est ce qu'un « cookie » ?

Un « cookie » petit fichier déposé sur le disque dur à l'insu de l'internaute, lors de la consultation de certains sites web, et qui conserve des informations en vue d'une connexion ultérieure. 

Les cookies contiennent des informations d'état renvoyées au serveur. Ces informations peuvent être un identifiant de session, une langue, une date d'expiration, etc. Elles permettent à {{NAME}} d’améliorer le contenu et les services proposés par le site, ainsi que de de faciliter la navigation.

Pour plus d'informations sur ce qu'est un cookie, vous pouvez visiter le [site Internet de la CNIL](https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser).

### 9.2. Quels cookies utilise-t-on ?

Le site {{INTRAMWET_NAME}} utilise plusieurs cookies : 
- Pour la page d'accueil : 
    - Cookie CSRF : contient une valeur aléatoire permettant de protéger le site contre les attaques de [Falsification de requête inter-site](https://www.squarefree.com/securitytips/web-developers.html#CSRF) (Cross Site Request Forgery - ou CSRF - en anglais) ;
    - Cookie d'acceptation de la politique des cookies : permet d'attester que l'utilisateur a accepté la politique d'utilisation des cookies ;
- Pour les pages internes, en plus des cookies définis ci-dessus : 
    - Cookie de session : permet d'attester que l'utilisateur est connecté, en évitant la reconnexion à chaque visite du site

### 9.3. Consentement

Lors de votre première visite du site {{INTRAMWET_NAME}}, vous êtes informés qu'en poursuivant votre navigation, vous acceptez l'installation de cookies sur votre terminal. 

Vous pourrez revenir sur votre choix via plusieurs méthodes : 

- en supprimant le cookie __cookielaw_accepted__
- via les paramètres de votre navigateur (voir les tutoriels pour [Firefox](https://support.mozilla.org/fr/kb/activer-desactiver-cookies-preferences?redirectlocale=fr&redirectslug=activer-desactiver-cookies), [Chrome](https://support.google.com/chrome/answer/95647?hl=fr), [Safari](https://support.apple.com/kb/PH17191?locale=en_US&viewlocale=fr_FR), [Opera](https://support.apple.com/kb/PH17191?locale=en_US&viewlocale=fr_FR), [iOS](https://support.apple.com/fr-fr/HT201265), [Android](https://www.wikihow.com/Disable-Cookies#Android_Devices))
    
Si l’Utilisateur refuse l’enregistrement de Cookies dans son terminal ou son navigateur, ou si l’Utilisateur supprime ceux qui y sont enregistrés, l’Utilisateur est informé que sa navigation et son expérience sur le Site peuvent être limitées. 

Enfin, en cliquant sur les icônes dédiées aux réseaux sociaux Twitter, Facebook et Youtube figurant sur le Site medialamouette.fr ou dans l'application mobile et si l’Utilisateur a accepté le dépôt de cookies en poursuivant sa navigation, Twitter, Facebook et Youtube peuvent également déposer des cookies sur vos terminaux.

## ARTICLE 10 : Évolution des conditions générales d'utilisation

Le site {{INTRAMWET_NAME}} se réserve le droit de modifier les clauses de ces conditions générales d’utilisation à tout moment et sans justification.

## ARTICLE 11 : Durée du contrat

La durée du présent contrat est indéterminée. Le contrat produit ses effets à l'égard de l'Utilisateur à compter du début de l’utilisation du service.

## ARTICLE 12 : Droit applicable et attribution de juridiction.

Les présentes conditions générales d'utilisation sont soumises au droit français. En cas de litige non résolu à l’amiable entre l’Utilisateur et l’Éditeur, les tribunaux de Lyon sont compétents pour régler le contentieux.