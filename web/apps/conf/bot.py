import random
from urllib.parse import urljoin

import django_rq
from django_rq import job
from django.core.cache import cache
from html2text import HTML2Text
from render_block import render_block_to_string, BlockNotFound
import json

import requests
from django.core.mail import EmailMultiAlternatives
from django.conf import settings


def get_context():
    from apps.conf.context_processors import intramwet_settings
    return intramwet_settings()


def get_prefix():
    prefix = cache.get('NOTIFICATION_PREFIX')
    if prefix:
        return prefix
    if settings.PRODUCTION:
        prefix = 'https://' + settings.ALLOWED_HOSTS[0][1:]
    else:
        prefix = 'http://' + settings.ALLOWED_HOSTS[0]
    cache.set('NOTIFICATION_PREFIX', prefix)
    return prefix


@job
def send_email_job(email):
    msg = EmailMultiAlternatives(email.subject, email.text, email.from_email,
                                 [email.to_email])
    msg.attach_alternative(email.html, "text/html")
    msg.send()


class Email:
    template = "base_mail.html"
    from_email = settings.DEFAULT_FROM_EMAIL

    def __init__(self, template, to_email=None, data={}):
        context = get_context()
        if to_email is None:
            to_email = context['CONTACT_EMAIL']
        data = {'logo': urljoin(get_prefix(), context['LOGO_PNG'].url), **context, **data}
        self.to_email = to_email
        self.template = template
        self.subject = render_block_to_string(self.template, 'subject', data)
        self.text = render_block_to_string(self.template, 'text', data)
        self.html = render_block_to_string(self.template, 'html', data)

        if self.text == '':
            h = HTML2Text()
            h.ignore_images = True
            h.ignore_emphasis = True
            h.ignore_tables = True
            self.text = h.handle(self.html)

    def send_mass(self, to_emails):
        for email in to_emails:
            self.to_email = email
            self.send()

    def send(self):
        if settings.PRODUCTION:
            send_email_job.delay(self)
        else:
            Bot('email', data={'subject': self.subject, 'text': self.text,
                               'from_email': self.from_email,
                               'to_email': self.to_email}).send()

    def delay(self, date=None, delta=None):
        scheduler = django_rq.get_scheduler()
        if date is not None:
            scheduler.enqueue_at(date, send_email_job, self)
        elif delta is not None:
            scheduler.enqueue_in(delta, send_email_job, self)


@job
def send_bot(bot):
    requests.post(bot.channel, headers=bot.headers,
                  data=json.dumps({"content": bot.text}))


class Bot:
    CHAN_BOT_DEV = settings.CHAN_BOT_DEV
    CHAN_BOT_PROD = settings.CHAN_BOT_PROD
    CHAN_BUREAU = settings.CHAN_BUREAU
    CHAN_MEMBRE = settings.CHAN_MEMBRE

    def __init__(self, template, channel='', data={}):
        context = get_context()
        self.headers = {
            "User-Agent": f"DiscordBot ({context['INTRAMWET_URL']}, v0.1)",
            "Content-Type": "application/json"
        }
        data = {'prefix': get_prefix(), **context, **data}
        if settings.PRODUCTION:
            self.channel = self.CHAN_BOT_PROD if channel == '' else channel
        else:
            self.channel = self.CHAN_BOT_DEV
        self.template = template
        try:
            render_block_to_string('bot.html', self.template + "_1", data)
            max = 2
            try:
                while True:
                    render_block_to_string('bot.html',
                                           self.template + "_{}".format(max),
                                           data)
                    max += 1
            except BlockNotFound:
                msg = render_block_to_string('bot.html',
                                             self.template + "_{}".format(
                                                 random.randint(1, max - 1)),
                                             data)
        except BlockNotFound:
            msg = render_block_to_string('bot.html', self.template, data)
        self.text = " ".join(msg.split()).replace('<br/>', '\n').replace('\n ',
                                                                         '\n')

    def send(self):
        send_bot.delay(self)

    def delay(self, date=None, delta=None):
        scheduler = django_rq.get_scheduler()
        if date is not None:
            scheduler.enqueue_at(date, send_bot, self)
        elif delta is not None:
            scheduler.enqueue_in(delta, send_bot, self)
