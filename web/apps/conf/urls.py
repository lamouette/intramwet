from django.urls import path

from .views import cgu

urlpatterns = [
    path('cgu/', cgu, name='cgu'),
]
