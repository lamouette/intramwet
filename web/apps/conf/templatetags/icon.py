from django import template

from ..context_processors import intramwet_settings

register = template.Library()


@register.inclusion_tag('icons.html')
def icon(icon_type: str, size: str = ""):
    props = intramwet_settings()
    return {
        'type': icon_type,
        'size': size,
        'INTRAMWET_EQUIPEMENT_ICON': props['INTRAMWET_EQUIPEMENT_ICON'],
        'INTRAMWET_PROJECT_ICON': props['INTRAMWET_PROJECT_ICON']
    }
