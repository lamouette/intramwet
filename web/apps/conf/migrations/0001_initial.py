# Generated by Django 3.1 on 2020-09-12 11:54

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AssociationGraphics',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('logo_png', models.ImageField(upload_to='public/logos/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['png'])], verbose_name='Logo en .png (à afficher sur fond blanc)')),
                ('logo_jpg', models.ImageField(upload_to='public/logos/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['jpg'])], verbose_name='Logo en .jpg (sur fond blanc, affiché sur les documents PDF)')),
                ('logo_negatif_png', models.ImageField(upload_to='public/logos/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['png'])], verbose_name='Logo négatif en .png (à afficher sur fond sombre)')),
                ('og_image_jpg', models.ImageField(upload_to='public/og/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['jpg'])], verbose_name='Image utilisée par le protocole OpenGraph pour représenter le site dans la recherche (format 464x886px)')),
                ('favicon_16_png', models.ImageField(upload_to='public/favicon/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['png'])], verbose_name='Favicon au format 16x16px (en .png)')),
                ('favicon_32_png', models.ImageField(upload_to='public/favicon/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['png'])], verbose_name='Favicon au format 32x32px (en .png)')),
                ('favicon_150_png', models.ImageField(upload_to='public/favicon/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['png'])], verbose_name='Favicon au format 150x150px (en .png)')),
                ('favicon_180_png', models.ImageField(upload_to='public/favicon/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['png'])], verbose_name='Favicon au format 180x180px (en .png)')),
                ('favicon_192_png', models.ImageField(upload_to='public/favicon/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['png'])], verbose_name='Favicon au format 192x192px (en .png)')),
                ('favicon_512_png', models.ImageField(upload_to='public/favicon/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['png'])], verbose_name='Favicon au format 512x512px (en .png)')),
                ('favicon_pinned_svg', models.ImageField(upload_to='public/favicon/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['svg'])], verbose_name='Favicon au format .svg (pour les fenêtres épinglées)')),
                ('favicon_ico', models.ImageField(upload_to='public/favicon/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['ico'])], verbose_name='Favicon au format .ico')),
            ],
        ),
        migrations.CreateModel(
            name='AssociationLegalProperties',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name="Nom de l'association")),
                ('short_name', models.CharField(blank=True, max_length=200, null=True, verbose_name="Nom court de l'association")),
                ('legal_status', models.CharField(max_length=200, verbose_name="Statut juridique de l'assocaition")),
                ('siren', models.BigIntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(999999999)], verbose_name="Numéro SIREN de l'association")),
                ('siret', models.BigIntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(99999999999999)], verbose_name="Numéro SIRET de l'association")),
                ('address', models.TextField(verbose_name="Lieu-dit de l'adresse juridique de l'association")),
                ('address_city', models.CharField(max_length=50, verbose_name="Ville de l'adresse juridique de l'association")),
                ('address_postal_code', models.IntegerField(verbose_name="Code postal de l'adresse juridique de l'association")),
                ('address_postal_code_cedex', models.BooleanField(default=False, verbose_name="Si le code postal de l'adresse juridique est un code CEDEX")),
                ('address_country', models.CharField(max_length=50, verbose_name="Pays de l'adresse juridique de l'association")),
                ('association_articles', models.FileField(blank=True, null=True, upload_to='public/documents/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['pdf'])], verbose_name="Statuts de l'association en PDF")),
                ('association_bylaw', models.FileField(blank=True, null=True, upload_to='public/documents/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['pdf'])], verbose_name="Règlement intérieur de l'association en PDF")),
                ('short_description', models.CharField(max_length=200, verbose_name="Description courte de l'association")),
            ],
        ),
        migrations.CreateModel(
            name='AssociationSocialProperties',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contact_email', models.EmailField(max_length=254, verbose_name='Adresse mail de contact')),
                ('support_email', models.EmailField(max_length=254, verbose_name='Adresse mail de support')),
                ('office_address', models.TextField(verbose_name="Addresse du local de l'association")),
                ('permanence', models.TextField(blank=True, null=True, verbose_name="Horaires de permanence de l'association")),
                ('meetup', models.TextField(blank=True, null=True, verbose_name="Horaires de réunion de l'association")),
                ('youtube_channel', models.URLField(blank=True, null=True, verbose_name="Chaine YouTube de l'association")),
                ('twitter_account', models.URLField(blank=True, null=True, verbose_name="Compte Twitter de l'association")),
                ('facebook_account', models.URLField(blank=True, null=True, verbose_name="Page Facebook de l'association")),
                ('instagram_account', models.URLField(blank=True, null=True, verbose_name="Compte Instagram de l'association")),
                ('website', models.URLField(blank=True, null=True, verbose_name="Site vitrine de l'association")),
            ],
        ),
        migrations.CreateModel(
            name='WebsiteManifests',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('webmanifest', models.FileField(upload_to='public/manifests/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['webmanifest'])], verbose_name='Web app manifest')),
                ('browserconfig', models.FileField(upload_to='public/manifests/%Y/%m/%d/', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['xml'])], verbose_name='Browser configuration file')),
            ],
        ),
        migrations.CreateModel(
            name='WebsiteProperties',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('intramwet_name', models.CharField(max_length=200, verbose_name='Nom du site web')),
                ('intramwet_homepage_activated', models.BooleanField(default=False, verbose_name="Si la page d'accueil est activée ou on redirige directement vers la page de connexion")),
                ('intramwet_equipement_icon', models.CharField(max_length=50, verbose_name='Icône utilisée pour le module de gestion de matériel')),
                ('intramwet_project_icon', models.CharField(max_length=50, verbose_name='Icône utilisée pour le module de gestion de projets')),
                ('intramwet_url', models.URLField(verbose_name='URL de déploiement du site')),
                ('intramwet_hosting_company_name', models.CharField(max_length=200, verbose_name="Nom de l'hébergeur du site")),
                ('intramwet_hosting_company_address', models.TextField(verbose_name="Coordonnées de l'hébergeur du site")),
                ('intramwet_hosting_location', models.CharField(max_length=200, verbose_name="Pays d'hébergement du site")),
            ],
        ),
    ]
