from django.conf import settings
from django.forms import model_to_dict


def export_vars(request=None):
    """Makes variables accessible in Django templates as {{VARIABLE_NAME}}"""
    return {
        'PRODUCTION': settings.PRODUCTION,
        'SENTRY_DSN': settings.SENTRY_DSN,
        'SENTRY_ENVIRONMENT': settings.SENTRY_ENVIRONMENT,
        'SENTRY_USER_INFO': settings.SENTRY_USER_INFO
    }


def intramwet_settings(request=None):
    """Makes all properties available in Django templates as {{VARIABLE_NAME}}"""
    from .models import WebsiteProperties, AssociationSocialProperties, AssociationGraphics, AssociationLegalProperties, WebsiteManifests
    association_legal_props = model_to_dict(AssociationLegalProperties.objects.last())
    association_social_props = model_to_dict(AssociationSocialProperties.objects.last())
    association_graphics = model_to_dict(AssociationGraphics.objects.last())
    website_props = model_to_dict(WebsiteProperties.objects.last())
    website_manifests = model_to_dict(WebsiteManifests.objects.last())
    props = {**association_legal_props, **association_social_props, **association_graphics, **website_props, **website_manifests}
    if not props['short_name']:
        props['short_name'] = props['name']
    return {key.upper(): value for key, value in props.items()}
