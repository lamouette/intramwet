from django.urls import path

from . import views
from .passation_action import passation_action_handler

urlpatterns = passation_action_handler.urls
urlpatterns.extend([
    path('modifier', views.passation, {'modifier': True}, name='passation'),
    path('', views.passation, name='passation')
])

print(urlpatterns)
