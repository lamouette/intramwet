from typing import Callable

from django.contrib.admin.sites import AlreadyRegistered
from django.urls import path, reverse
from django.utils.module_loading import autodiscover_modules


class PassationAction:
    def __init__(self, handler_view: Callable, url: str, name: str = None, verbose_name: str = None, fa_icon: str = None, btn_class: str = "btn-default", confirm: bool = True):
        self.handler_view = handler_view
        self.url = url
        self.name = name or handler_view.__name__
        self.verbose_name = verbose_name or self.name
        self.fa_icon = fa_icon
        self.btn_class = btn_class
        self.confirm = confirm


class PassationActionHandler(object):
    def __init__(self):
        self._registry = {}

    def register(self, action: PassationAction):
        assert isinstance(action, PassationAction)

        if action.name in self._registry:
            raise AlreadyRegistered

        self._registry[action.name] = action

    @property
    def actions(self):
        return [PassationAction(a.handler_view, reverse(a.name), a.name, a.verbose_name, a.fa_icon, a.btn_class, a.confirm) for a in self._registry.values()]

    @property
    def urls(self):
        return [path(a.url, a.handler_view, name=a.name) for a in self._registry.values()]


passation_action_handler = PassationActionHandler()
autodiscover_modules("views", register_to=passation_action_handler)
