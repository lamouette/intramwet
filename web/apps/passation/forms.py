from django.forms import modelformset_factory, forms

from apps.utilisateur.models import MembreCA

BaseMembreCAFormset = modelformset_factory(
    MembreCA,
    fields=('statut', 'membre'),
    min_num=1,
    extra=0
)


class MembreCAFormset(BaseMembreCAFormset):
    def clean(self):
        if any(self.errors):
            return

        postes_uniques = []
        for form in self.forms:
            statut = form.cleaned_data['statut']
            if statut.unique:
                if statut in postes_uniques:
                    raise forms.ValidationError(
                        "On ne peut avoir qu'un seul poste de %s" % statut.statut)
                postes_uniques.append(statut)
