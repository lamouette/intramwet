from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.core.exceptions import PermissionDenied


def can_access_treso(function):
    def wrap(request, *args, **kwargs):
        func = login_required(function)
        if request.user.has_perm('utilisateur.acceder_treso') or request.user.has_perm('utilisateur.represente_organisation'):
            return func(request, *args, **kwargs)
        else:
            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def can_manage_treso(function):
    def wrap(request, *args, **kwargs):
        return staff_member_required(can_access_treso(function))(request, *args, **kwargs)
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def can_access_organisation(function):
    def wrap(request, *args, **kwargs):
        func = can_access_treso(function)
        if request.user.has_perm('utilisateur.represente_organisation'):
            return func(request, *args, **kwargs)
        else:
            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap