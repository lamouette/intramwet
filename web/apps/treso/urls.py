from django.urls import path

from .views import grille_tarifaire, modifier_entree_grille, \
    supprimer_entree_grille, facture_emprunt, emprunts, \
    telecharger_factures, telecharger_devis, telecharger_notes_de_frais, \
    note_de_frais, supprimer_note_de_frais, \
    generer_note_de_frais, generer_devis, supprimer_devis, generer_facture, \
    supprimer_facture, treso, get_facture, get_note_de_frais, \
    facture, devis, liste_client, edit_client, creer_client, \
    supprimer_client, mon_organisation, \
    demande_organisation, generer_organisation_client_particulier


urlpatterns = [
    path('facture/emprunt/<int:id_emprunt>/save/', facture_emprunt,
         {'sauvegarder': True},
         name='facture_emprunt_sauvegarder'),
    path('facture/emprunt/<int:id_emprunt>/', facture_emprunt,
         name='facture_emprunt'),
    path('facture/emprunt/', emprunts, name="emprunts_facturables"),

    path('facture/telecharger/<int:facture_id>', get_facture,
         name="telecharger_facture"),
    path('facture/telecharger/', telecharger_factures,
         name='telecharger_factures'),
    path('facture/generer/<int:id_facture>/', generer_facture,
         name='generer_facture'),
    path('facture/supprimer/<int:id_facture>/', supprimer_facture,
         name='supprimer_facture'),
    path('facture/save/', facture, {'sauvegarder': True},
         name='facture_sauvegarder'),
    path('facture/', facture, name='facture'),

    path('devis/telecharger/', telecharger_devis, name='telecharger_devis'),
    path('devis/generer/<int:id_devis>/', generer_devis, name='generer_devis'),
    path('devis/supprimer/<int:id_devis>/', supprimer_devis,
         name='supprimer_devis'),
    path('devis/save/', devis, {'sauvegarder': True},
         name='devis_sauvegarder'),
    path('devis/', devis, name='devis'),

    path('ndf/telecharger/<int:ndf_id>', get_note_de_frais,
         name="telecharger_ndf"),
    path('ndf/telecharger/', telecharger_notes_de_frais,
         name='telecharger_notes_de_frais'),
    path('ndf/generer/<int:id_ndf>/', generer_note_de_frais,
         name='generer_note_de_frais'),
    path('ndf/supprimer/<int:id_ndf>/', supprimer_note_de_frais,
         name='supprimer_note_de_frais'),
    path('ndf/save/', note_de_frais, {'sauvegarder': True},
         name='note_de_frais_sauvegarder'),
    path('ndf/', note_de_frais, name='note_de_frais'),

    path('client/liste/', liste_client, name='client'),
    path('client/<int:id_client>/', edit_client, name='edit_client'),
    path('client/', creer_client, name='create_client'),
    path('client/supprimer/<int:id_client>/', supprimer_client,
         name='supprimer_client'),

    path('organisation/', mon_organisation, name='mon_organisation'),
    path('organisation/demande', demande_organisation,
         name='demande_organisation'),
    path('organisation/creation/particulier/<int:id_utilisateur>/',
         generer_organisation_client_particulier,
         name="generer_organisation_client_particulier"),

    path('grille/', grille_tarifaire, name="grille_tarifaire"),
    path('grille/modifier/<int:id_entree>', modifier_entree_grille,
         name="modifier_entree_grille"),
    path('grille/ajouter/', modifier_entree_grille,
         name="ajouter_entree_grille"),
    path('grille/supprimer/<int:id_entree>', supprimer_entree_grille,
         name="supprimer_entree_grille"),

    path('', treso, name="treso")
]
