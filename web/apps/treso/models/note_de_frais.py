from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.utils.functional import cached_property

from apps.conf.utils import format_file_title
from apps.utilisateur.models import Utilisateur
from .utils import TresoAbstraite


class NoteDeFrais(TresoAbstraite):
    """Note de frais

    Une NoteDeFrais est composée d'ElementNoteDeFrais.
    """
    collaborateur = models.ForeignKey(Utilisateur, on_delete=models.PROTECT,
                                      related_name="notes_de_frais")
    nature = models.CharField("Nature des frais", max_length=200)

    ESP = "ESP"
    CHQ = "CHQ"
    VIR = "VIR"
    methode_remboursement = models.CharField(
        "Méthode de remboursement",
        max_length=3,
        choices=[
            (ESP, "En espèces"),
            (CHQ, "Par chèque"),
            (VIR, "Par virement")
        ],
        default=CHQ
    )
    numero_cheque = models.CharField("Numéro du chèque", max_length=200,
                                     null=True, blank=True)
    fichier = models.FileField(upload_to='ndf/', null=True, blank=True)

    @cached_property
    def nom_fichier(self):
        return format_file_title(
            'N{}_{}_.pdf'.format(self.numero, self.collaborateur.last_name,
                                 self.nature))

    @cached_property
    def total(self):
        return sum(e.montant for e in
                   self.elements.all()) if self.elements.all() else 0

    def __str__(self):
        return "%d | %s | %s" % (self.numero, self.collaborateur, self.nature)


class NoteDeFraisElement(models.Model):
    """Une entrée de note de frais

    Une entrée est caractérisée par le type de frais, ainsi que par son montant
    """
    ndf = models.ForeignKey(NoteDeFrais, on_delete=models.CASCADE,
                            related_name="elements")
    frais = models.CharField("Frais", max_length=200)
    montant = models.FloatField("Montant en €")

    def __str__(self):
        return "%s | %s | %d€" % (self.ndf, self.frais, self.montant)


@receiver(post_delete, sender=NoteDeFrais)
def remove_file_on_delete(sender, instance: NoteDeFrais, **kwargs):
    instance.fichier.delete(False)
