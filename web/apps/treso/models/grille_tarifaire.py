from django.db import models

from . import FactureElement


class CategorieGrilleTarifaire(models.Model):
    nom = models.CharField("Nom de la catégorie", max_length=50)

    def __str__(self):
        return self.nom


class EntreeGrilleTarifaire(models.Model):
    nom = models.CharField("Nom de la prestation", max_length=200)
    description = models.CharField("Description de la prestation",
                                   max_length=600, null=True, blank=True)
    categorie = models.ForeignKey(to=CategorieGrilleTarifaire,
                                  on_delete=models.CASCADE, null=True,
                                  blank=True)
    prix = models.FloatField("Prix de la prestation (€)")

    def est_lie(self):
        return FactureElement.objects.filter(prestation=self).exists()

    def __str__(self):
        return self.nom
