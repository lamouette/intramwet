from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.utils.functional import cached_property

from apps.conf.utils import format_file_title
from . import Client
from .utils import FactureAbstraite, FactureElementAbstrait


class Devis(FactureAbstraite):
    client = models.ForeignKey(Client, on_delete=models.PROTECT,
                               related_name="devis")
    fichier = models.FileField(upload_to='devis/', null=True, blank=True)

    @cached_property
    def nom_fichier(self):
        return format_file_title('D{}{}.pdf'.format(self.numero, '_{}'.format(
            self.label) if self.label else ''))

    @cached_property
    def total(self):
        return Devis.calc_total(self.elements.all())


class DevisElement(FactureElementAbstrait):
    facture = models.ForeignKey(Devis, on_delete=models.CASCADE,
                                related_name='elements')

    def __str__(self):
        return "%s | %s | %d€ | %d" % (
            self.facture, self.nom, self.prix, self.quantite)


@receiver(post_delete, sender=Devis)
def remove_file_on_delete(sender, instance: Devis, **kwargs):
    instance.fichier.delete(False)
