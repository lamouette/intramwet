from django.core.validators import RegexValidator
from django.db import models


class Client(models.Model):
    """Clients de l'association

    Cette table permet de sauvegarder les données des clients, afin de ne pas avoir à les re-rentrer à chaque fois
    """
    nom = models.CharField("Nom de l'organisation", max_length=200)
    particulier = models.BooleanField("Le client est un particulier",
                                      default=False)
    adresse = models.CharField("Adresse de l'organisation", max_length=200)
    adresse_seconde_ligne = models.CharField("Deuxième ligne d'adresse",
                                             max_length=200, null=True,
                                             blank=True)
    ville = models.CharField("Ville de l'organisation", max_length=200,
                             null=True)
    code_postal = models.CharField("Code postal", max_length=200, null=True)
    email = models.EmailField("Adresse mail de l'organisation", null=True,
                              blank=True)
    telephone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                     message="Le numéro de téléphone doit être entré au format '+999999999'. 15 chiffres maximum.")
    telephone = models.CharField('numéro de téléphone',
                                 validators=[telephone_regex], max_length=17,
                                 null=True, blank=True)

    def est_lie(self):
        return self.factures.exists()

    def __str__(self):
        return self.nom
