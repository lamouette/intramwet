# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Client, Devis, DevisElement, Facture, FactureElement, CategorieGrilleTarifaire, EntreeGrilleTarifaire, NoteDeFrais, NoteDeFraisElement


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = (
        'nom',
        'particulier',
        'email',
    )
    list_filter = ('particulier',)


@admin.register(Devis)
class DevisAdmin(admin.ModelAdmin):
    list_display = (
        'numero',
        'date',
        'client',
        'fichier',
    )
    list_filter = ('date', 'client')


@admin.register(DevisElement)
class DevisElementAdmin(admin.ModelAdmin):
    list_display = (
        'nom',
        'quantite',
        'prix',
        'coefficient',
        'facture',
    )
    list_filter = ('facture',)


@admin.register(Facture)
class FactureAdmin(admin.ModelAdmin):
    list_display = (
        'numero',
        'date',
        'client',
        'fichier',
    )
    list_filter = ('date', 'client')


@admin.register(FactureElement)
class FactureElementAdmin(admin.ModelAdmin):
    list_display = (
        'nom',
        'quantite',
        'prix',
        'coefficient',
        'facture',
    )
    list_filter = ('facture',)


@admin.register(CategorieGrilleTarifaire)
class CategorieGrilleTarifaireAdmin(admin.ModelAdmin):
    list_display = ('nom',)


@admin.register(EntreeGrilleTarifaire)
class EntreeGrilleTarifaireAdmin(admin.ModelAdmin):
    list_display = ('nom', 'categorie', 'prix')
    list_filter = ('categorie',)


@admin.register(NoteDeFrais)
class NoteDeFraisAdmin(admin.ModelAdmin):
    list_display = (
        'numero',
        'date',
        'collaborateur',
        'nature',
        'fichier',
    )
    list_filter = ('date', 'collaborateur')


@admin.register(NoteDeFraisElement)
class NoteDeFraisElementAdmin(admin.ModelAdmin):
    list_display = ('ndf', 'frais')
    list_filter = ('ndf',)

