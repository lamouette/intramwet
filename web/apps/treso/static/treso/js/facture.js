function utiliserGrilleTarifaire(nom, prix) {
    var lastRow = $('.form-row:last');
    lastRow.find('.nom').val(nom);
    lastRow.find('.prix').val(prix);
}

function send_form(url) {
    var form = $('form');
    if (form[0].checkValidity()) {
        form.attr("action", url);
        form.submit();
    } else form[0].reportValidity();
}

$(function () {
    $('#id_date').datetimepicker({
        locale: 'fr',
        format: 'DD/MM/YYYY'
    });

    $('.switch').click(function () {
        $(this).find('i').toggleClass("fa-angle-right fa-angle-down");
    });

});