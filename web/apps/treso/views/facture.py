from django.db.models import ProtectedError
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect

from apps.conf.utils import get_zipped_response, get_schoolyear, media_response
from ..decorators import can_manage_treso
from ..forms import FactureForm, FactureElementFormset
from ..models import Facture, FactureElement, EntreeGrilleTarifaire
from . import traiter_facture, render_pdf_response
from apps.passation.decorators import can_access_passation, register


@can_manage_treso
def facture(request, sauvegarder: bool = False) -> HttpResponse:
    """ Permet de générer une facture libre.

    Utilise les Formset Django pour relier les éléments à la facture (https://medium.com/@taranjeet/adding-forms-dynamically-to-a-django-formset-375f1090c2b0)
    :param request:
    :param sauvegarder: si on doit sauvegarder la facture, ou juste la générer et renvoyer
    :return:
    """

    template_name = 'treso/facture.html'

    if request.method == 'POST':
        factureform = FactureForm(request.POST)
        formset = FactureElementFormset(request.POST)
        return traiter_facture(factureform=factureform, formset=formset,
                               sauvegarder=sauvegarder)

    dernier_numero = Facture.dernier_numero()
    factureform = FactureForm(request.GET or None)
    formset = FactureElementFormset(queryset=FactureElement.objects.none())
    grille_tarifaire = EntreeGrilleTarifaire.objects.all().order_by(
        "categorie", "nom")
    return render(request, template_name, locals())


@can_manage_treso
def generer_facture(request, id_facture: int) -> HttpResponse:
    """ Permet de générer le PDF associé à une facture libre déjà sauvegardée

    :param request:
    :param id_facture: l'id de la facture libre
    :return: le PDF de la facture
    """
    facture = get_object_or_404(Facture, id=id_facture)
    return render_pdf_response('treso/rml/facture.rml', facture.nom_fichier,
                               locals())


@can_manage_treso
def supprimer_facture(request, id_facture: int) -> HttpResponse:
    """ Permet de supprimer une facture libre de la liste des factures

    :param request:
    :param id_facture: id de la facture à supprimer
    :return:
    """
    try:
        facture = get_object_or_404(Facture, id=id_facture)
        facture.delete()
    except ProtectedError:
        pass
    return redirect('treso')


@can_manage_treso
def get_facture(request, facture_id: int):
    """
    Permet au bureau de récupérer une facture

    :param request:
    :param facture_id: id de l'objet Facture à télécharger
    :return:
    """
    facture = Facture.objects.get(id=facture_id)
    return media_response(facture.fichier.url)


@can_manage_treso
def telecharger_factures(request) -> HttpResponse:
    files = [f.fichier.path for f in Facture.objects.all() if f.fichier]
    return get_zipped_response(file_list=files,
                               archive_name=f'{get_schoolyear()}_factures.zip')


@register(url='factures/supprimer', name='supprimer_factures_toutes',
          verbose_name="Supprimer toutes les factures",
          fa_icon="fas fa-trash", btn_class="btn-danger delete")
@can_access_passation
def supprimer_factures(request):
    """ Supprime toutes les factures

    :param request:
    :return:
    """
    for facture in Facture.objects.all():
        facture.delete()
    return redirect('passation')
