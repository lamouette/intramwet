from django.http import HttpResponse
from django.shortcuts import redirect, render

from apps.materiel.models import EmpruntMateriel
from ..decorators import can_manage_treso
from ..forms import FactureEmpruntForm
from ..models import Facture, FactureElement
from . import save_pdf, render_pdf_response


@can_manage_treso
def facture_emprunt(request, id_emprunt: int,
                    sauvegarder: bool = False) -> HttpResponse:
    """ Permet de générer une facture liée à un emprunt déjà effectué.

    Prends en compte le prix correspondant à l'utilisateur (selon son groupe)
    :param request:
    :param id_emprunt: id de l'emprunt
    :param sauvegarder: si on doit sauvegarder la facture, ou juste la renvoyer
    :return:
    """

    if request.method == "POST":
        form = FactureEmpruntForm(data=request.POST)
        if form.is_valid():
            coefficient = form.cleaned_data.get("coefficient")
            emprunt = EmpruntMateriel.objects.get(id=id_emprunt)
            facture = Facture(
                numero=Facture.dernier_numero() + 1,
                client=emprunt.membre.organisation,
                motif="Location de materiel du {} au {}".format(
                    emprunt.debut.strftime("%Y-%m-%d"),
                    emprunt.fin.strftime("%Y-%m-%d"))
            )
            emprunt_elements_payants = emprunt.elements_payants()
            if sauvegarder:
                facture.save()
                for emprunt_element in emprunt_elements_payants:
                    element = FactureElement(
                        nom=emprunt_element.materiel.nom +
                            " (location pour 1 jour)",
                        quantite=emprunt_element.quantite * emprunt.nb_jours_emprunt,
                        prix=emprunt_element.materiel.prixLocation,
                        coefficient=coefficient,
                        facture=facture
                    )
                    element.save()

                facture.fichier.save(facture.nom_fichier,
                                     save_pdf('treso/rml/facture.rml',
                                              locals()))
                return render_pdf_response('treso/rml/facture.rml',
                                           facture.nom_fichier, locals())
            else:
                elements = []
                for emprunt_element in emprunt_elements_payants:
                    elements.append({
                        'prestation': emprunt_element.materiel.nom + " (location pour 1 jour)",
                        'quantite': emprunt_element.quantite * emprunt.nb_jours_emprunt,
                        'prix': emprunt_element.materiel.prixLocation,
                        'coefficient': coefficient})

                total = sum(
                    e['quantite'] * e['prix'] * e['coefficient'] for e in
                    elements)
                return render_pdf_response(
                    'treso/rml/facture_sans_sauvegarde.rml',
                    facture.nom_fichier, locals())
    return redirect("emprunts_facturables")


@can_manage_treso
def emprunts(request) -> HttpResponse:
    """ Permet de voir tous les emprunts susceptibles à être facturés

    :param request:
    :return:
    """
    emprunts_payants = [e for e in
                        EmpruntMateriel.objects.all().order_by('-id') if
                        e.elements_payants()]
    form = FactureEmpruntForm()
    return render(request, 'treso/facture_emprunts.html', locals())
