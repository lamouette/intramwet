import zipfile

from django.contrib.auth import get_user_model
from django.http import HttpResponse
from django.shortcuts import render

from ..decorators import can_manage_treso
from ..models import Facture, Devis, NoteDeFrais
from apps.passation.decorators import register, can_access_passation
from ...conf.utils import add_to_zipfile, get_schoolyear


@can_manage_treso
def treso(request) -> HttpResponse:
    """ Permet d'afficher l'ensemble des factures libres, factures d'emprunts, factures de prestations et notes de frais

    :param request:
    :return:
    """
    factures = Facture.objects.all().order_by('-numero')
    devis = Devis.objects.all().order_by('-numero')
    ndfs = NoteDeFrais.objects.all().order_by('-numero')
    return render(request, 'treso/treso.html', locals())


@register(url='documents/telecharger', name='telecharger_documents',
          verbose_name="Télécharger tous les documents de l'année administrative",
          fa_icon="fas fa-download", btn_class="btn-primary", confirm=False)
@can_access_passation
def telecharger_documents(request) -> HttpResponse:
    from apps.projet.models import FichierTache
    from apps.treso.models import Facture, NoteDeFrais, Devis

    factures = [f.fichier.path for f in Facture.objects.all() if f.fichier]
    devis = [d.fichier.path for d in Devis.objects.all() if d.fichier]
    ndf = [n.fichier.path for n in NoteDeFrais.objects.all() if n.fichier]
    fiches_inscription = [u.ficheInscription.fichier.path for u in
                          get_user_model().objects.all() if
                          u.ficheInscription and u.ficheInscription.fichier]
    fiches_cgu = [u.ficheCGU.path for u in get_user_model().objects.all() if
                  u.ficheCGU]
    fiches_cession_droits = [f.fichier.path for f in FichierTache.objects.all()
                             if f.fichier]

    response = HttpResponse(content_type='application/zip')
    response[
        'Content-Disposition'] = f'attachment; filename={get_schoolyear()}_documents.zip'
    with zipfile.ZipFile(response, 'w') as z:
        add_to_zipfile(zip_file=z, file_list=factures, folder_name="factures")
        add_to_zipfile(zip_file=z, file_list=devis, folder_name="devis")
        add_to_zipfile(zip_file=z, file_list=ndf, folder_name="ndf")
        add_to_zipfile(zip_file=z, file_list=fiches_inscription,
                       folder_name="fiches_inscription")
        add_to_zipfile(zip_file=z, file_list=fiches_cgu,
                       folder_name="fiches_cgu")
        add_to_zipfile(zip_file=z, file_list=fiches_cession_droits,
                       folder_name="fiches_cession_droits")

    return response
