from django.db.models import ProtectedError
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect

from ..decorators import can_manage_treso
from ..forms import EntreeGrilleTarifaireForm
from ..models import EntreeGrilleTarifaire


@can_manage_treso
def grille_tarifaire(request) -> HttpResponse:
    """ Interface de gestion de la grille tarifaire

    :param request:
    :return:
    """
    entrees = EntreeGrilleTarifaire.objects.all().order_by('categorie', 'nom')
    return render(request, 'treso/grille_tarifaire.html', locals())


@can_manage_treso
def modifier_entree_grille(request, id_entree: int = None) -> HttpResponse:
    """ Permet de modifier une entrée de la grille tarifaire

    :param request:
    :param id_entree: id de l'entrée à modifier
    :return:
    """

    if id_entree:
        entree = get_object_or_404(EntreeGrilleTarifaire, id=id_entree)
        template_name = 'treso/modifier_entree_grille.html'
    else:
        entree = None
        template_name = 'treso/creer_entree_grille.html'

    form = EntreeGrilleTarifaireForm(
        request.POST if request.method == 'POST' else None, instance=entree)

    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('grille_tarifaire')

    return render(request, template_name, locals())


@can_manage_treso
def supprimer_entree_grille(request, id_entree: int) -> HttpResponse:
    """ Permet au bureau de supprimer une entrée de la grille tarifaire

    :param request:
    :param id_entree: id de l'entrée à supprimer
    :return:
    """
    try:
        entree = get_object_or_404(EntreeGrilleTarifaire, id=id_entree)
        entree.delete()
    except ProtectedError:
        pass

    return redirect('grille_tarifaire')
