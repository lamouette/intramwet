from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
from django.db.models import ProtectedError
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404

from apps.conf.bot import Bot
from ..decorators import can_access_treso, can_manage_treso, \
    can_access_organisation
from ..forms import ClientForm
from ..models import Client


@can_manage_treso
def liste_client(request) -> HttpResponse:
    """ Interface de gestion de tous les clients

    :param request:
    :return:
    """
    clients = Client.objects.all().order_by('nom')
    return render(request, 'treso/clients.html', locals())


@can_manage_treso
def generer_organisation_client_particulier(request,
                                            id_utilisateur: int) -> HttpResponse:
    """ Permet de générer une organisation portant le nom du particulier, en reprenant ses coordonnées

    :param request:
    :param id_utilisateur: id de l'utilisateur
    :return:
    """
    utilisateur = get_user_model().objects.get(id=id_utilisateur)
    utilisateur.organisation, _ = Client.objects.get_or_create(
        nom=utilisateur.nomComplet, adresse=utilisateur.adresse,
        email=utilisateur.email,
        telephone=utilisateur.numeroDeTelephone,
        particulier=True)
    utilisateur.save()
    return redirect('admin_user', id_utilisateur)


@can_access_organisation
def mon_organisation(request) -> HttpResponse:
    """ Permet à un utilisateur de gérer son organisation
    :param request:
    :return:
    """
    organisation = request.user.organisation
    return render(request, 'treso/mon_organisation.html',
                  {'organisation': organisation})


@can_access_treso
def edit_client(request, id_client: int) -> HttpResponse:
    """ Permet au bureau de modifier l'organisation associée à un client, ou au client de modifier sa propre organisation

    :param request:
    :param id_client: l'id du client à modifier
    :return:
    """
    client = get_object_or_404(Client, id=id_client)
    if not request.user.is_staff and request.user.organisation != client:
        raise PermissionDenied
    form = ClientForm(request.POST if request.method == 'POST' else None,
                      instance=client)

    if request.method == 'POST':
        if form.is_valid():
            form.save()
            if request.user.is_staff:
                return redirect('client')
            else:
                return redirect('mon_organisation')

    return render(request, 'treso/edit_client.html', locals())


@can_manage_treso
def creer_client(request) -> HttpResponse:
    """ Permet au bureau de créer l'organisation associée à un client

    :param request:
    :return:
    """
    form = ClientForm(request.POST if request.method == 'POST' else None)

    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('client')

    return render(request, 'treso/edit_client.html', locals())


@can_manage_treso
def supprimer_client(request, id_client: int) -> HttpResponse:
    """ Permet au bureau de supprimer un client

    :param request:
    :param id_client: id du client à supprimer
    :return:
    """
    try:
        client = get_object_or_404(Client, id=id_client)
        client.delete()
    except ProtectedError:
        pass

    return redirect('client')


@can_access_organisation
def demande_organisation(request) -> HttpResponse:
    """ Informe le bureau qu'un utilisateur souhaite intégrer une organisation lors de l'inscription

    :param request:
    :return:
    """
    if request.method == 'POST':
        Bot('demande_organisation', channel=Bot.CHAN_BUREAU, data={
            'user': request.user,
            'msg': request.POST['organisation']
        }).send()
    return JsonResponse({})
