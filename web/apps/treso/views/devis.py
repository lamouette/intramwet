from django.db.models import ProtectedError
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect

from apps.conf.utils import get_zipped_response, get_schoolyear
from ..decorators import can_manage_treso
from ..forms import DevisForm, DevisElementFormset
from ..models import Devis, FactureElement, EntreeGrilleTarifaire
from . import traiter_facture, render_pdf_response
from apps.passation.decorators import can_access_passation, register


@can_manage_treso
def devis(request, sauvegarder: bool = False) -> HttpResponse:
    """ Permet de générer un devis

    :param request:
    :param sauvegarder: si on doit sauvegarder le devis, ou juste le générer et renvoyer
    :return:
    """

    template_name = 'treso/devis.html'

    if request.method == 'POST':
        factureform = DevisForm(request.POST)
        formset = DevisElementFormset(request.POST)
        return traiter_facture(factureform=factureform, formset=formset,
                               sauvegarder=sauvegarder, devis=True)

    dernier_numero = Devis.dernier_numero()
    factureform = DevisForm(request.GET or None)
    formset = DevisElementFormset(queryset=FactureElement.objects.none())
    grille_tarifaire = EntreeGrilleTarifaire.objects.all().order_by(
        "categorie", "nom")
    return render(request, template_name, locals())


@can_manage_treso
def generer_devis(request, id_devis: int) -> HttpResponse:
    """ Permet de générer le PDF associé à une facture libre déjà sauvegardée

    :param request:
    :param id_devis: l'id du devis à générer
    :return: le PDF du devis
    """
    facture = get_object_or_404(Devis, id=id_devis)
    devis = True
    return render_pdf_response('treso/rml/facture.rml', facture.nom_fichier,
                               locals())


@can_manage_treso
def supprimer_devis(request, id_devis: int) -> HttpResponse:
    """ Permet de supprimer un devis

    :param request:
    :param id_devis: id du devis à supprimer
    :return:
    """
    try:
        devis = get_object_or_404(Devis, id=id_devis)
        devis.delete()
    except ProtectedError:
        pass
    return redirect('treso')


@can_manage_treso
def telecharger_devis(request) -> HttpResponse:
    files = [f.fichier.path for f in Devis.objects.all() if f.fichier]
    return get_zipped_response(file_list=files,
                               archive_name=f'{get_schoolyear()}_devis.zip')


@register(url='devis/supprimer', name='supprimer_devis_tous',
          verbose_name="Supprimer tous les devis",
          fa_icon="fas fa-trash", btn_class="btn-danger delete")
@can_access_passation
def supprimer_devis_tous(request):
    """ Supprime tous les devis

    :param request:
    :return:
    """
    for devis in Devis.objects.all():
        devis.delete()
    return redirect('passation')
