from django.apps import AppConfig


class TresoConfig(AppConfig):
    name = 'apps.treso'
