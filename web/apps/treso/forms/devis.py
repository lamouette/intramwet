from django.forms import modelformset_factory

from apps.conf.utils import get_schoolyear
from . import FactureForm
from ..models import Devis, DevisElement


class DevisForm(FactureForm):
    class Meta:
        model = Devis
        fields = ['numero', 'motif', 'label', 'client', 'date']
        help_texts = {
            'numero': "Numéro du devis au format %4dXXX (sans préfixe D)" % (
                get_schoolyear()),
            'date': "Date d'édition du devis",
            'label': 'Nom court sans espace donné au fichier PDF',
            'motif': 'Motif imprimé sur le devis',
        }


DevisElementFormset = modelformset_factory(
    DevisElement,
    fields=('nom', 'quantite', 'prix', 'coefficient'),
    extra=1
)
