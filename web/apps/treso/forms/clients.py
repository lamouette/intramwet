from django import forms

from ..models import Client


class ClientForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ClientForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
        self.fields['nom'].widget.attrs.update({'placeholder': 'Nom'})
        self.fields['adresse'].widget.attrs.update(
            {'placeholder': "1ère ligne d'adresse",
             'value': "20, avenue Albert Einstein"})
        self.fields['adresse_seconde_ligne'].widget.attrs.update(
            {'placeholder': "2ème ligne d'adresse (Facultatif)"})
        self.fields['ville'].widget.attrs.update(
            {'placeholder': "Ville", 'value': "Villeurbanne"})
        self.fields['code_postal'].widget.attrs.update(
            {'placeholder': "Code Postal", 'value': "69100"})
        self.fields['email'].widget.attrs.update(
            {'placeholder': "E-Mail (Facultatif)"})
        self.fields['telephone'].widget.attrs.update(
            {'placeholder': "Téléphone (Facultatif)"})

    class Meta:
        model = Client
        fields = ['nom', 'adresse', 'adresse_seconde_ligne', 'ville',
                  'code_postal', 'email', 'telephone']
        help_texts = {
            'nom': "Nom à imprimer sur la facture",
            'adresse': "Ex.: INSA Lyon Casier du BdE",
            'adresse_seconde_ligne': "Ex.: 20, avenue Albert Einstein",
            'ville': "Ex.: Villeurbanne Cedex",
            'code_postal': "Ex.: 69621",
        }
