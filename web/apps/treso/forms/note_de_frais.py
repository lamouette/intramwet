from django import forms
from django.forms import modelformset_factory

from apps.conf.utils import get_schoolyear
from ..models import NoteDeFrais, NoteDeFraisElement


class NoteDeFraisForm(forms.ModelForm):
    class Meta:
        model = NoteDeFrais
        fields = ['numero', 'collaborateur', 'nature', 'date',
                  'methode_remboursement', 'numero_cheque']
        help_texts = {
            'numero': "Numéro de la note de frais au format %4dXXX (sans préfixe N)" % (
                get_schoolyear()),
            'date': "Date d'édition de la note de frais",
            'nature': 'Nature des frais remboursés',
            'numero_cheque': 'Seulement si remboursée par chèque'
        }


NoteDeFraisElementFormset = modelformset_factory(
    NoteDeFraisElement,
    fields=('frais', 'montant'),
    extra=1
)
