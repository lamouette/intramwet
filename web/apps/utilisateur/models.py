import os
from datetime import datetime, timezone, date

from django.contrib.auth.models import AbstractUser, Group, Permission
from django.core.validators import RegexValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.templatetags.static import static
from django.utils.functional import cached_property
from icalendar import vCalAddress, vText


class Utilisateur(AbstractUser):
    anniversaire = models.DateField('anniversaire', null=True, blank=True)
    adresse = models.TextField('adresse', max_length=200, null=True,
                               blank=True)
    numeroDeTelephoneRegex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                            message="Le numéro de téléphone doit être entré au format '+999999999'. 15 chiffres maximum.")
    numeroDeTelephone = models.CharField('numéro de téléphone',
                                         validators=[numeroDeTelephoneRegex],
                                         max_length=17,
                                         null=True, blank=True)
    photo = models.ImageField(upload_to='photos_utilisateurs/', blank=True,
                              null=True)
    montantCaution = models.IntegerField('montant de la caution', default=0)
    numeroChequeDeCaution = models.IntegerField('numero de cheque', blank=True,
                                                null=True)

    ficheCGU = models.FileField(upload_to='cgu/', null=True, blank=True)

    valide = models.BooleanField("membre validé par le bureau", default=False)
    actif = models.BooleanField("membre actif pour l'année", default=True)

    NON_PAYEE = "ATT"
    PAYEE_ESP = "PES"
    PAYEE_CHQ = "PCH"
    PAYEE_VIR = "PVI"
    NON_NECESSAIRE = "INU"
    statutCotisation = models.CharField(
        max_length=3,
        choices=[
            (NON_PAYEE, "Non payée"),
            (PAYEE_ESP, "Payée en espèces"),
            (PAYEE_CHQ, "Payée par chèque"),
            (PAYEE_VIR, "Payée par virement"),
            (NON_NECESSAIRE, "Non nécessaire"),
        ],
        default=NON_NECESSAIRE,
    )

    organisation = models.ForeignKey('treso.Client', on_delete=models.PROTECT,
                                     null=True, blank=True,
                                     related_name='utilisateurs')

    def __str__(self):
        return "%s %s @%s" % (self.first_name, self.last_name, self.username)

    @cached_property
    def nomComplet(self):
        return "%s %s" % (self.first_name, self.last_name)

    @cached_property
    def photo_url(self):
        return self.photo.url if self.photo else static(
            'utilisateur/images/default-user.png')

    @cached_property
    def ficheInscription(self):
        today = datetime.now(timezone.utc)
        year = int(today.strftime('%Y'))
        if today.month < 9:
            debut = datetime(year=year - 1, month=9, day=1)
            fin = datetime(year=year, month=8, day=31, hour=23, minute=59,
                           second=59)
        else:
            debut = datetime(year=year, month=9, day=1)
            fin = datetime(year=year + 1, month=8, day=31, hour=23, minute=59,
                           second=59)
        return self.fichesInscription.all().filter(date__lt=fin,
                                                   date__gte=debut).order_by(
            '-date').first()

    @cached_property
    def vCalAddress(self):
        utilisateur = vCalAddress(f'MAILTO:{self.email}')
        utilisateur.params['cn'] = vText(self.nomComplet)
        return utilisateur

    @property
    def montant_cotisation(self) -> int:
        if self.statutCotisation != self.NON_NECESSAIRE:
            return CotisationGroupe.objects.get_for_user(user=self).cotisation
        return 0

    class Meta:
        permissions = (
            ('acceder_materiel', 'Peut accéder à la page materiel'),
            ('acceder_local', 'Peut accéder à la page d\'emprunt du local'),
            ('acceder_projets', 'Peut accéder à la page projets'),
            ('acceder_passation', "Peut accéder à l'interface de passation."),
            ('acceder_membres', "Peut accéder au module membres."),
            ('acceder_treso', "Peut accéder au module de tréso."),
            ('acceder_wiki', "Peut accéder au module wiki."),
            ('fiche_inscription',
             'Doit générer une fiche d\'inscription à la validation'),
            ('represente_organisation',
             'Peut représenter un client de l\'association')
        )


class StatutCA(models.Model):
    statut = models.CharField("Statut du membre au Conseil d'administration",
                              max_length=200)
    unique = models.BooleanField("Statut unique du poste CA", default=False)
    acceder_passation = models.BooleanField("Ce statut donne accès à l'interface de passation", default=False)
    acceder_treso = models.BooleanField("Ce statut donne accès à la tréso", default=False)
    acceder_su = models.BooleanField("Ce statut donne un accès super-utilisateur", default=False)

    PRESIDENT = "PRES"
    TRESORIER = "TRES"
    SECRETAIRE = "SECR"
    DEV = "DEV"
    AUTRE = "AUTR"
    GESTIONNAIRE_CINET = "CINT"
    poste = models.CharField(
        max_length=4,
        choices=[
            (PRESIDENT, "Président"),
            (TRESORIER, "Trésorier"),
            (SECRETAIRE, "Sécretaire"),
            (DEV, "Développeur"),
            (AUTRE, "Autre"),
            (GESTIONNAIRE_CINET, "Gestionnaire cinet'")
        ],
        default=AUTRE,
    )

    def __str__(self):
        return "%s" % self.statut


class MandatCA(models.Model):
    debut = models.DateField("Date de début du mandat", auto_now_add=True)
    fin = models.DateField("Date de fin du mandat", null=True, blank=True)

    @property
    def current(self) -> bool:
        return self.fin is None or self.fin > datetime.today().date()

    def __str__(self):
        return "Bureau actuel" if self.current else f"Du {self.debut} au {self.fin}"


@receiver(post_save, sender=MandatCA)
def remove_admin_rights_for_old_members(sender, instance: MandatCA,
                                        created: bool, **kwargs):
    for m in instance.membresCA.all():
        m.save(force_update=True)


class MembreCA(models.Model):
    statut = models.ForeignKey(StatutCA, on_delete=models.PROTECT)
    membre = models.ForeignKey(Utilisateur, on_delete=models.CASCADE,
                               related_name="postesCA")
    mandat = models.ForeignKey(MandatCA, on_delete=models.CASCADE,
                               related_name="membresCA")

    @property
    def current(self) -> bool:
        return self.mandat.current

    def set_admin_rights(self, save: bool = True):
        self.membre.is_staff = True
        self.membre.is_superuser = self.statut.acceder_su
        if self.statut.acceder_passation:
            permission = Permission.objects.get(codename='acceder_passation').id
            self.membre.user_permissions.add(permission)
        if self.statut.acceder_treso:
            permission = Permission.objects.get(codename='acceder_treso').id
            self.membre.user_permissions.add(permission)
        self.membre.save()
        if save:
            self.save()

    def remove_admin_rights(self, save: bool = True):
        if not self.membre.is_superuser:
            self.membre.is_staff = False
            self.membre.is_superuser = False
            if self.membre.has_perm('utilisateur.acceder_passation'):
                permission = Permission.objects.get(codename='acceder_passation').id
                self.membre.user_permissions.remove(permission)
            if self.membre.has_perm('utilisateur.acceder_treso'):
                permission = Permission.objects.get(codename='acceder_treso').id
                self.membre.user_permissions.remove(permission)
            self.membre.save()
            if save:
                self.save()

    def __str__(self):
        return "%s | %s" % (self.statut, self.membre)


@receiver(post_save, sender=MembreCA)
def set_admin_rights_on_membre_CA_saved(sender, instance: MembreCA,
                                        created: bool, **kwargs):
    if instance.mandat.current:
        instance.set_admin_rights(save=False)
    else:
        instance.remove_admin_rights(save=False)


class DescriptionGroupe(models.Model):
    group = models.OneToOneField(Group, on_delete=models.CASCADE,
                                 related_name='description')
    description = models.TextField("description du groupe", max_length=200)
    demandePrecisions = models.BooleanField(
        "nécessite des précisions supplémentaires", default=False)

    def __str__(self):
        return self.group.name


class CotisationGroupeManager(models.Manager):
    def get_for_user(self, user: Utilisateur):
        return self.filter(group__in=user.groups.all()).order_by('cotisation').first()


class CotisationGroupe(models.Model):
    group = models.OneToOneField(Group, on_delete=models.CASCADE,
                                 related_name='cotisation')
    cotisation = models.IntegerField(default=0)
    objects = CotisationGroupeManager()

    def __str__(self):
        return self.group.name


class FicheInscription(models.Model):
    utilisateur = models.ForeignKey(Utilisateur, on_delete=models.CASCADE,
                                    related_name='fichesInscription')
    fichier = models.FileField(upload_to='fiches_inscription/')
    date = models.DateField('Date de génération')

    def __str__(self):
        return "%s | %s" % (
            self.utilisateur.nomComplet, os.path.basename(self.fichier.name))

    @staticmethod
    def next_id():
        today = datetime.now(timezone.utc)
        if today.month < 9:
            debut = datetime(year=today.year - 1, month=9, day=1)
            fin = datetime(year=today.year, month=8, day=31, hour=23,
                           minute=59, second=59)
        else:
            debut = datetime(year=today.year, month=9, day=1)
            fin = datetime(year=today.year + 1, month=8, day=31, hour=23,
                           minute=59, second=59)
        return "I{}{}{:0>3}".format(debut.strftime('%y'), fin.strftime('%y'),
                                    len(FicheInscription.objects.filter(
                                        date__lt=fin, date__gte=debut)) + 1)
