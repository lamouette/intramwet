from datetime import datetime, timezone
from tempfile import TemporaryFile

from django.contrib.auth import authenticate, update_session_auth_hash, logout, \
    get_user_model
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import Group
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, render, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse
from xhtml2pdf import pisa

from apps.treso.views import save_pdf
from apps.conf.bot import Email, Bot
from apps.conf.utils import media_response
from apps.conf.views import get_cgu_text
from .decorators import can_access_membres, can_manage_membres
from .forms import InscriptionForm, ModificationForm, \
    AdminModificationForm, UsernameForgottenForm, \
    PasswordResetForm
from .models import Utilisateur, FicheInscription, MandatCA
from apps.passation.decorators import can_access_passation, register

##########################################################################
#                                                                        #
# Interface d'inscription, connexion et déconnexion                      #
#                                                                        #
##########################################################################


def inscription(request) -> HttpResponse:
    """ Interface d'inscription

    Utilise un formulaire asynchrone avec jQuery.
    :param request:
    :return: JsonResponse avec un code de succès et les erreurs éventuelles
    """

    if request.method == 'POST':
        form = InscriptionForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            group = form.cleaned_data.get('groups')
            user = authenticate(username=username, password=raw_password)
            user.groups.add(group)
            user.save()
            generer_fiche_cgu(request, user)
            if user.groups.all:
                if user.groups.all()[0].description.demandePrecisions:
                    precisions = request.POST['precisions']
            Email('emails/inscription.html', user.email, locals()).send()
            Bot('inscription', channel=Bot.CHAN_BUREAU, data=locals()).send()
            return JsonResponse({
                "valid": True
            })

        return JsonResponse({
            "valid": False,
            "errors": render_to_string('registration/signup_errors.html',
                                       {'form': form})
        })

    form = InscriptionForm()
    return render(request, 'registration/signup.html',
                  {'form': form, 'next': reverse('accueil', kwargs={
                      'modal_inscription': True})})


def nom_oublie(request) -> HttpResponse:
    """ Interface de réinitialisation du nom d'utilisateur

    Renvoie un mail contenant tous les noms d'utilisateur associés à l'adresse mail spécifiée.
    :param request:
    :return:
    """
    if request.method == "POST":
        form = UsernameForgottenForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get("email")
            users = Utilisateur.objects.filter(email=email)
            Email('emails/username_forgotten.html', email, locals()).send()
            return redirect('accueil', modal_nom_utilisateur=True)
    form = UsernameForgottenForm()
    return render(request, 'registration/username_forgotten.html', locals())


class PasswordResetView(auth_views.PasswordResetView):
    """
        Surcharge de la vue de reset de mdp pour pouvoir envoyer un mail avec le moteur de mail interne
    """
    form_class = PasswordResetForm


def reset_complete(request) -> HttpResponse:
    """ Vue de confirmation de réinitialisation de mot de passe

    :param request:
    :return:
    """
    return redirect('connexion')


@login_required
def deconnexion(request) -> HttpResponse:
    """ Vue de déconnexion

    Déconnecte l'utilisateur et le renvoie à la pagge d'accueil
    :param request:
    :return:
    """
    logout(request)
    return redirect('accueil')


##########################################################################
#                                                                        #
# Profil utilisateur                                                     #
#                                                                        #
##########################################################################

@login_required
def user_view(request) -> HttpResponse:
    """ Profil utilisateur

    :param request:
    :return: le profil de l'utilisateur connecté
    """
    if request.method == 'GET':
        password_form = PasswordChangeForm(user=request.user)
        info_form = ModificationForm(instance=request.user)
        return render(request, 'profil.html', {
            'user': request.user,
            'password_form': password_form,
            'info_form': info_form
        })

    return redirect('accueil')


@login_required
def password_change(request) -> HttpResponse:
    """ Vue qui gère la modification du mot de passe

    :param request:
    :return: le profil utilisateur, une fois le mot de passe changé
    """
    if request.method == 'POST':
        password_form = PasswordChangeForm(data=request.POST,
                                           user=request.user)
        info_form = ModificationForm(instance=request.user)
        if password_form.is_valid():
            password_form.save()
            update_session_auth_hash(request, password_form.user)
            return redirect('utilisateur')

        return render(request, 'profil.html', {
            'user': request.user,
            'password_form': password_form,
            'info_form': info_form
        })

    return redirect('utilisateur')


@login_required
def user_info_change(request) -> HttpResponse:
    """ Vue qui gère la modification des informations de l'utilisateur

    :param request:
    :return: le profil utilisateur, une fois ses informations modifiées
    """
    if request.method == 'POST':
        info_form = ModificationForm(request.POST, request.FILES,
                                     instance=request.user)
        password_form = PasswordChangeForm(user=request.user)
        if info_form.is_valid():
            info_form.save()
            return redirect('utilisateur')

        return render(request, 'profil.html', {
            'user': request.user,
            'password_form': password_form,
            'info_form': info_form
        })

    return redirect('utilisateur')


@can_access_membres
def all_users(request) -> HttpResponse:
    """ Interface avec tous les utilisateurs du site

    Sont exclus les utilisateurs inactifs (dans le sens de Django), non-validés et le compte superuser (medialamouette)
    :param request:
    :return:
    """
    users = Utilisateur.objects.all().exclude(
        Q(is_active=False) | Q(actif=False) | Q(valide=False)
    ).order_by('groups', '-is_staff','last_name')
    return render(request, 'all_users.html', {'users': users})


@can_access_membres
def administrators(request) -> HttpResponse:
    """ Interface avec tous les bureaux
    :param request:
    :return:
    """
    mandats = MandatCA.objects.order_by('-id')
    return render(request, 'bureaux.html', locals())


@can_access_membres
def public_profile(request, uuid: int):
    user = get_object_or_404(Utilisateur, id=uuid)
    return render(request, 'public_profile.html', locals())


@can_access_membres
def get_user_profile_picture(request, user_id: int):
    """
    Permet aux utilisateurs connectés de récupérer les photos de profil des autres utilisateurs (et, entre autre, leur
    propre photo de profil)

    :param request:
    :param user_id: identifiant de l'utilisateur dont on veut récupérer l'image
    :return:
    """
    user = get_object_or_404(Utilisateur, id=user_id)
    return media_response(path=user.photo_url)


##########################################################################
#                                                                                                                      #
# Interface d'administration des membres                                                                               #
#                                                                                                                      #
##########################################################################

@can_manage_membres
def admin_all_users(request) -> HttpResponse:
    """ Interface avec tous les utilisateurs du site

    Sont exclus les utilisateurs inactifs (dans le sens de Django) et le compte superuser (medialamouette)
    :param request:
    :return:
    """
    users = Utilisateur.objects.all().exclude(
        Q(actif=False) | Q(is_active=False)
    ).order_by('valide', 'groups','last_name')
    return render(request, 'admin_all_users.html', {'users': users})


@can_manage_membres
def admin_delete_user(request, uuid: int) -> HttpResponse:
    """ Désactive un compte utilisateur

    On désactive le compte à la place de le supprimer complètement pour garder l'historique de ses emprunts, les
    factures, etc (bref qui sont des clés étrangères d'autres modèles). En fin d'année, tous les comptes inactifs
    peuvent être définitivement supprimés.
    :param request:
    :param uuid: identifiant de l'utilisateur à supprimer
    :return:
    """
    user = get_object_or_404(Utilisateur, id=uuid)
    if not user.is_superuser:
        user.is_active = False
        user.save()
    return redirect('admin_all_users')


@can_manage_membres
def admin_user(request, uuid: int) -> HttpResponse:
    """ Interface de modification d'un utilisateur par le bureau

    :param request:
    :param uuid: identifiant de l'utilisateur à modifier
    :return:
    """
    utilisateur = get_object_or_404(Utilisateur, id=uuid)
    if request.method == 'POST':
        old_organisation = utilisateur.organisation
        form = AdminModificationForm(request.POST, request.FILES,
                                     instance=utilisateur)
        if form.is_valid():
            if old_organisation != form.cleaned_data.get(
                    'organisation') and form.cleaned_data.get('organisation'):
                Email('emails/changement_organisation.html', utilisateur.email,
                      {
                          'user': utilisateur,
                          'organisation': utilisateur.organisation
                      }).send()
            form.save()
            return redirect('admin_user', uuid=uuid)

    form = AdminModificationForm(instance=utilisateur)
    groups = Group.objects.all()
    return render(request, 'admin_user.html',
                  {'user': utilisateur, 'form': form, 'groups': groups,
                   'NON_PAYEE': Utilisateur.NON_PAYEE,
                   'PAYEE_ESP': Utilisateur.PAYEE_ESP,
                   'PAYEE_VIR': Utilisateur.PAYEE_VIR,
                   'PAYEE_CHQ': Utilisateur.PAYEE_CHQ,
                   'NON_NECESSAIRE': Utilisateur.NON_NECESSAIRE})


@can_manage_membres
def valider(request, uuid: int, groupid: int) -> HttpResponse:
    """ Permet de valider un compte utilisateur

    :param request:
    :param uuid: identifiant de l'utilisateur à valider
    :param groupid: identifiant du groupe qu'il veut rejoindre
    :return:
    """
    user = get_object_or_404(Utilisateur, id=uuid)
    group = get_object_or_404(Group, id=groupid)
    user.valide = True
    user.groups.clear()
    user.groups.add(group)
    if user.has_perm('utilisateur.fiche_inscription'):
        user.statutCotisation = Utilisateur.NON_PAYEE
    user.save()
    Email('emails/validation.html', user.email, locals()).send()
    return redirect('admin_user', uuid=uuid)


@can_manage_membres
def valider_cotisation(request, uuid: int, cotisid: str) -> HttpResponse:
    """ Valide le paiement de la cotisation de la part de l'utilisateur

    Après avoir valider le paiement, génère la fiche d'inscription de l'utilisateur
    :param request:
    :param uuid: identifiant de l'utilisateur
    :param cotisid: moyen de paiement de la cotisation
    :return:
    """
    user = get_object_or_404(Utilisateur, id=uuid)
    if cotisid in [Utilisateur.PAYEE_ESP, Utilisateur.PAYEE_VIR,
                   Utilisateur.PAYEE_CHQ]:
        user.statutCotisation = cotisid
        user.save()
        if user.has_perm('utilisateur.fiche_inscription'):
            generer_fiche_inscription(user)
    elif cotisid == Utilisateur.NON_NECESSAIRE:
        user.statutCotisation = cotisid
        user.save()
    return redirect('admin_user', uuid=uuid)


##########################################################################
#                                                                        #
# Fiches d'inscription et d'acceptation des CGU                          #
#                                                                        #
##########################################################################

def generer_fiche_inscription(user: Utilisateur):
    """ Génère la fiche d'inscription d'un utilisateur

    :param user: objet Utilisateur utilisé pour la génération de la fiche
    :return: void
    """
    today = datetime.now(timezone.utc)
    no_inscription = FicheInscription.next_id()
    filename = '{}_{}{}.pdf'.format(no_inscription,
                                    user.first_name.capitalize()[0],
                                    user.last_name.capitalize())
    fiche = FicheInscription(date=today.date(), utilisateur=user)
    fiche.fichier.save(filename, save_pdf('fiche_inscription.rml',
                                          {'user': user, 'today': today,
                                           'no_inscription': no_inscription,
                                           'PAYEE_ESP': Utilisateur.PAYEE_ESP,
                                           'PAYEE_VIR': Utilisateur.PAYEE_VIR,
                                           'PAYEE_CHQ': Utilisateur.PAYEE_CHQ}))
    fiche.save()


def generer_fiche_cgu(request, user):
    """ Génère la fiche d'acceptation des CGU d'un utilisateur

    :param user: objet Utilisateur utilisé pour la génération de la fiche
    :return: void
    """
    today = datetime.now(timezone.utc)
    year = int(today.strftime('%y'))
    no_inscription = "I{}{}{:0>3}".format(
        year - 1 if today.month < 9 else year,
        year if today.month < 9 else year + 1,
        user.id)
    filename = 'CGU_{}_{}{}.pdf'.format(no_inscription,
                                        user.first_name.capitalize()[0],
                                        user.last_name.capitalize())
    with TemporaryFile(mode='w+b') as f:
        html_data = render_to_string('fiche_cgu.html',
                                     {'user': user, 'today': today,
                                      'no_inscription': no_inscription,
                                      'cgu': get_cgu_text()})
        pisa.CreatePDF(html_data.encode('utf-8'), dest=f, encoding='utf-8')
        f.seek(0)
        user.ficheCGU.save(filename, f)
        user.save()


@login_required
def get_user_inscription(request, user_id: int):
    """
    Permet à l'utilisateur de récupérer sa propre fiche d'inscription. Le bureau peut récupérer les fiches de
    tous les utilisateurs

    :param request:
    :param user_id: identifiant de l'utilisateur dont on veut récupérer la fiche d'inscription
    :return:
    """
    user = get_object_or_404(Utilisateur, id=user_id)
    if user == request.user or request.user.is_staff:
        return media_response(user.ficheInscription.fichier.url)
    raise PermissionDenied


##########################################################################
#                                                                        #
# Actions de passation                                                   #
#                                                                        #
##########################################################################


@register(url='inactifs/supprimer', name='supprimer_anciens_membres',
          verbose_name="Supprimer les comptes des membres inactifs cette année",
          fa_icon="fas fa-user-slash", btn_class="btn-danger delete")
@can_access_passation
def supprimer_anciens_membres(request):
    """ Supprime tous les membres inactifs qui ne sont pas membres du bureau

    :param request:
    :return:
    """
    for user in get_user_model().objects.filter(actif=False, is_staff=False,
                                                is_superuser=False):
        user.delete()
    return redirect('passation')


@register(url='membres/desactiver', name='desactiver_membres',
          verbose_name="Désactiver les comptes des membres",
          fa_icon="fas fa-user-slash", btn_class="btn-danger delete")
@can_access_passation
def desactiver_membres(request) -> HttpResponse:
    """ Permet de désactiver tous les membres en fin d'année

    Rends les utilisateurs invalides, et réinitialise le statut de paiement de cotisation
    :param request:
    :return:
    """
    for user in get_user_model().objects.all():
        if not user.is_superuser and not user.is_staff:
            user.valide = False
            user.actif = False
        if user.statutCotisation != Utilisateur.NON_NECESSAIRE:
            user.statutCotisation = Utilisateur.NON_PAYEE
        user.save()
    return redirect('passation')


##########################################################################
#                                                                        #
# Conditions générales d'utilisation                                     #
#                                                                        #
##########################################################################


@login_required
def get_user_cgu_consent(request, user_id: int):
    """
    Permet à l'utilisateur de récupérer sa propre fiche d'acceptation des CGU. Le bureau peut récupérer les fiches de
    tous les utilisateurs

    :param request:
    :param user_id: identifiant de l'utilisateur dont on veut récupérer la fiche d'acceptation des CGU
    :return:
    """
    user = get_object_or_404(Utilisateur, id=user_id)
    if user == request.user or request.user.is_staff:
        return media_response(path=user.ficheCGU.url)
    raise PermissionDenied
