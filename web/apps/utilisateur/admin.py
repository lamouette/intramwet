# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Utilisateur, StatutCA, MandatCA, MembreCA, DescriptionGroupe, CotisationGroupe, FicheInscription


@admin.register(Utilisateur)
class UtilisateurAdmin(admin.ModelAdmin):
    list_display = (
        'username',
        'first_name',
        'last_name',
        'is_superuser',
        'is_staff',
        'valide',
        'actif',
        'organisation',
        'last_login',
    )
    list_filter = (
        'last_login',
        'is_superuser',
        'is_staff',
        'is_active',
        'valide',
        'actif',
        'organisation',
    )
    raw_id_fields = ('groups', 'user_permissions')


@admin.register(StatutCA)
class StatutCAAdmin(admin.ModelAdmin):
    list_display = (
        'statut',
        'unique',
        'acceder_passation',
        'acceder_treso',
        'acceder_su',
        'poste',
    )
    list_filter = (
        'unique',
        'acceder_passation',
        'acceder_treso',
        'acceder_su',
    )


@admin.register(MandatCA)
class MandatCAAdmin(admin.ModelAdmin):
    list_display = ('debut', 'fin')
    list_filter = ('debut', 'fin')


@admin.register(MembreCA)
class MembreCAAdmin(admin.ModelAdmin):
    list_display = ('statut', 'membre', 'mandat')
    list_filter = ('statut', 'membre', 'mandat')


@admin.register(DescriptionGroupe)
class DescriptionGroupeAdmin(admin.ModelAdmin):
    list_display = ('group', 'description', 'demandePrecisions')
    list_filter = ('group', 'demandePrecisions')


@admin.register(CotisationGroupe)
class CotisationGroupeAdmin(admin.ModelAdmin):
    list_display = ('group', 'cotisation')
    list_filter = ('group',)


@admin.register(FicheInscription)
class FicheInscriptionAdmin(admin.ModelAdmin):
    list_display = ('utilisateur', 'fichier', 'date')
    list_filter = ('utilisateur', 'date')

