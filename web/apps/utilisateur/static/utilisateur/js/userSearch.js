function search(value) {
    const resultsDiv = $("#searchbar-results");
    resultsDiv.empty();
    const isEmpty = (str) => (!str || 0 === str.length);
    if (!isEmpty(value)) {
        const users = $(".user-card");
        const getProperty = (user, property) => $(user).find(property).text().toLowerCase();
        const matchesValue = (user) => {
            const fullName = getProperty(user, ".user-fullName");
            if (fullName.includes(value))
                return true;
            const username = getProperty(user, ".user-username");
            if (username.includes(value))
                return true;
            const statutCA = getProperty(user, ".user-statutCA");
            if (statutCA.includes(value))
                return true;
            return false;
        }
        users.each((index, user) => {
            if (matchesValue(user))
                $(user).clone().appendTo(resultsDiv);
        })
    }
}