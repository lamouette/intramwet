function validate_email(text) {
    let email_regexp = /^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/;
    return email_regexp.test(text);
}

function sendFormIfValid() {
    if (validate_email(usernameField.val()) === false) $("form")[0].submit();
    else $("#email-used-as-username").show();
}
