$(document).ready(function () {
    $("#btn-tos").click(function (e) {
        if (validateStep($('.modal-body')["0"])) $("#myModal").modal("hide");
        else alert("Vous devez accepter les conditions générales d'utilisation pour continuer");
    });

    $("#myModal").modal('show');

    $('.has-popover').popover({'trigger': 'hover'});

    $('input[type=radio][name=groupHTMLName]').change(function () {
        if ($('input[name=groupHTMLName]:checked').data('precisions') === "True") {
            $('#precisions').show();
        } else {
            $('#precisions').hide();
        }
    });

});

function inscrire() {
    let form = $("#formInscription");
    if (form[0].checkValidity()) {
        if (validateStep($('.modal-body')["0"])) {
            form.ajaxSubmit({
                url: form.action,
                type: 'POST',
                success: function (response) {
                    if (response['valid']) {
                        let next = $('#next').val();
                        location.replace(next);
                    } else {
                        let error_field = $("#errors");
                        error_field.html(response['errors']);
                        error_field.css('display', 'block');
                        scrollTo(0, 0);
                    }
                }
            });
        } else {
            $('#myModal').modal('show');
            alert("Vous devez accepter les conditions générales d'utilisation pour continuer");
        }
    } else form[0].reportValidity();
}

function validateStep(elem) {
    let inputs = elem.getElementsByTagName('input');
    let valid = true;
    let i = 0;
    while (i < inputs.length && valid) {
        valid = inputs[i].checkValidity();
        i++;
    }
    return valid;
}

