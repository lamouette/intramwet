from django.contrib.auth import views as auth_views
from django.urls import path

from .forms import ConnexionForm
from .views import deconnexion, reset_complete, \
    admin_user, user_view, password_change, user_info_change, admin_all_users, \
    admin_delete_user, all_users, \
    public_profile, get_user_cgu_consent, get_user_inscription, \
    get_user_profile_picture, nom_oublie, inscription, PasswordResetView, \
    valider, valider_cotisation, administrators

urlpatterns = [
    path('', user_view, name='utilisateur'),
    path('nom_d_utilisateur/', nom_oublie, name="username"),
    path('inscription/fiche/<int:user_id>', get_user_inscription,
         name="fiche_inscription"),
    path('photo/<int:user_id>', get_user_profile_picture,
         name="photo_utilisateur"),
    path('cgu/fiche/<int:user_id>', get_user_cgu_consent, name="fiche_cgu"),
    path('tous/admin/', admin_all_users, name="admin_all_users"),
    path('bureaux/', administrators, name="administrators"),
    path('tous/', all_users, name="all_users"),
    path('<int:uuid>/public/', public_profile, name="public_profile"),
    path('modification/<int:uuid>', admin_user, name="admin_user"),
    path('suppression/<int:uuid>', admin_delete_user,
         name="admin_delete_user"),
    path('inscription/', inscription, name='inscription'),
    path('modification/', user_info_change, name='modification_compte'),
    path('connexion/',
         auth_views.LoginView.as_view(authentication_form=ConnexionForm),
         name='connexion'),
    path('deconnexion/', deconnexion, name='deconnexion'),
    path('mot_de_passe/', password_change, name='modification_mot_de_passe'),
    path('mot_de_passe/reinitialisation/', PasswordResetView.as_view(),
         {'post_reset_redirect': '/#/login?resetemail=true'},
         name="password_reset"),
    path('mot_de_passe/reinitialisation/ok/',
         auth_views.PasswordResetDoneView.as_view(),
         name='password_reset_done'),
    path('mot_de_passe/reinitialisation/confirmation/<uidb64>/<token>',
         auth_views.PasswordResetConfirmView.as_view(),
         name='password_reset_confirm'),
    path('mot_de_passe/reinitialisation/fini', reset_complete,
         name='password_reset_complete'),
    path('valider/<int:uuid>/<int:groupid>', valider,
         name='valider_utilisateur'),
    path('cotisation/<int:uuid>/<str:cotisid>', valider_cotisation,
         name='valider_cotisation'),
]
