from datetime import datetime

from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, \
    PasswordResetForm as auth_PasswordResetForm
from django.contrib.auth.models import Group
from django.contrib.auth.tokens import default_token_generator
from django.core.validators import RegexValidator
from django.forms import ModelForm, Form

from apps.conf.bot import Email
from .models import Utilisateur


class PhoneField(forms.CharField):
    default_validators = [RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                         message="Le numéro de téléphone doit être entré au format '+999999999'. 15 "
                                                 "chiffres maximum.")]


class InscriptionForm(UserCreationForm):
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    email = forms.EmailField(max_length=254)
    anniversaire = forms.DateField()
    numeroDeTelephone = PhoneField(max_length=17)
    prefix = 'inscription'

    class Meta:
        model = Utilisateur
        fields = (
            'username', 'first_name', 'last_name', 'email', 'password1',
            'password2', 'anniversaire',
            'numeroDeTelephone', 'photo', 'adresse', 'groups')

    def __init__(self, *args, **kwargs):
        super(InscriptionForm, self).__init__(*args, **kwargs)
        self.fields['groups'] = forms.ModelChoiceField(
            queryset=Group.objects.all())

    def conforme_age_minimal(self):
        date_anniversaire = self.cleaned_data['anniversaire']
        age_minimal = 16
        age_utilisateur = (
                                  datetime.now().date() - date_anniversaire).days / 365
        if age_utilisateur < age_minimal:
            self.add_error("anniversaire",
                           "Vous devez être âgé d'au moins 16 ans pour vous inscrire sur ce site")
            return False
        return True

    def is_valid(self):
        valid = super().is_valid()
        self.conforme_age_minimal()
        return valid and self.conforme_age_minimal()


class ConnexionForm(AuthenticationForm):
    def __init__(self, request=None, *args, **kwargs):
        super().__init__(request, *args, **kwargs)
        self.error_messages['invalid'] = "Votre compte n'est pas valide. Avant de pouvoir vous connecter, vous devez \
            attendre qu'un administrateur valide votre compte."

    def confirm_login_allowed(self, user):
        super().confirm_login_allowed(user)
        if not user.actif:
            user.actif = True
            user.save()

        if not user.valide:
            raise forms.ValidationError(
                self.error_messages['invalid'],
                code='invalid',
            )


class UsernameForgottenForm(Form):
    email = forms.EmailField(max_length=254)


class PasswordResetForm(auth_PasswordResetForm):
    """
        Surcharge du formulaire d'envoi de mail de reset de mdp pour pouvoir envoyer des mails en HTML
    """

    def save(self, domain_override=None,
             email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             request=None,
             email_subject_name='registration/password_reset_subject.txt',
             **kwargs):
        from django.utils.http import urlsafe_base64_encode
        from django.utils.encoding import force_bytes

        for user in self.get_users(self.cleaned_data["email"]):
            c = {
                'email': user.email,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'user': user,
                'token': token_generator.make_token(user),
            }
            Email('emails/password_reset_email.html', user.email, c).send()


class ModificationForm(ModelForm):
    class Meta:
        model = Utilisateur
        fields = ('email', 'numeroDeTelephone', 'adresse', 'photo')


class AdminModificationForm(ModelForm):
    class Meta:
        model = Utilisateur
        fields = (
            'email', 'anniversaire', 'numeroDeTelephone', 'photo', 'adresse',
            'numeroChequeDeCaution', 'montantCaution',
            'valide', 'is_staff', 'groups', 'organisation'
        )
