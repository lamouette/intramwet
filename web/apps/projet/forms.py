from django import forms
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.forms import JSONField
from django.forms import MultipleChoiceField

from .models import Projet, Tache, Creneau, TypeProjet


class TypeProjetForm(forms.ModelForm):
    class Meta:
        model = TypeProjet
        fields = ['typeProjet']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        choices = [(g.name, g.name) for g in Group.objects.all()]
        initial = []
        if self.instance:
            initial = [g.name for g in Group.objects.filter(
                permissions__codename=self.instance.permission_name)]
        self.fields['groupesAyantAcces'] = MultipleChoiceField(
            label="Groupes ayant accès",
            choices=choices,
            initial=initial,
            required=False
        )

    def save(self, commit=True):
        m = super().save(commit=False)
        if commit:
            m.save()

        permission = Permission.objects.get(
            codename=self.instance.permission_name,
            content_type=ContentType.objects.get_for_model(TypeProjet))

        for group in Group.objects.filter(permissions=permission):
            group.permissions.remove(permission)

        for group_name in self.cleaned_data['groupesAyantAcces']:
            group = Group.objects.get(name=group_name)
            group.permissions.add(permission)

        return m


class ProjetForm(forms.ModelForm):
    participants = JSONField(required=False)

    def __init__(self, *args, **kwargs):
        super(ProjetForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})

        self.fields['nom'].widget.attrs.update(
            {'placeholder': 'Nom du projet'})
        self.fields['description'].widget.attrs.update(
            {
                'placeholder': 'Décris ici ton projet pour le reste des membres !'})
        # self.fields['participants'].widget = forms.HiddenInput()
        self.fields['participants'].widget.attrs.update(
            {'style': 'display:none'})
        self.fields['participants'].label = ""
        self.fields['deadlineRendu'].widget.attrs.update(
            {'placeholder': '(Facultatif)'})

    class Meta:
        model = Projet
        fields = ['nom', 'type', 'deadlineRendu', 'description']
        help_texts = {
            'deadlineRendu': "Deadline de rendu global du projet, c'est-à-dire, quand rendre le montage terminé."
        }


class TacheForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TacheForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
            if self.fields[field].widget.__class__.__name__ == 'CheckboxInput':
                self.fields[field].widget.attrs.update(
                    {'class': 'form-check-input'})

        self.fields['nom'].widget.attrs.update(
            {'placeholder': 'Nom de la tâche'})
        self.fields['description'].widget.attrs.update(
            {'placeholder': 'Description de la tâche'})

    class Meta:
        model = Tache
        fields = ['nom', 'tacheTerminee', 'description', 'visible',
                  'maxMembreAssignable', 'ficheCessionDroits',
                  'notifPourInscription', 'notifBureauTermine',
                  'notifBureauAvantDate', 'notifMembreAvantDate',
                  'notifMembreApresDate', 'notifBureau', 'notifMembre']
        help_texts = {
            'maxMembreAssignable': "Régler à 0 pour interdire les inscriptions à la tâche.",

            'ficheCessionDroits': "Lorsqu'un utilisateur s'inscrit, génère une fiche de cession de droits d'auteur. Nécessaire pour toutes les captations.",

            'notifPourInscription': "Envoie régulièrement des notifications aux membres si personne ne s'inscrit à la tâche. Utile pour la plupart des tâches de captation.",
            'notifBureauTermine': "Envoie une notification au bureau et aux administrateurs du projet lorsque la tâche est terminée. Utile pour une tâche \"Montage\".",

            'notifBureauAvantDate': 'Si la tâche possède un créneau, notifie le bureau si personne n\'est inscrit juste avant la date. Utile pour une tâche "Captation".',
            'notifMembreAvantDate': 'Si la tâche possède un créneau, rappel aux membres inscrits la veille de la tâche. Utile pour une tâche "Captation"',
            'notifMembreApresDate': 'Si la tâche possède un créneau, notifie les membres inscrits pour savoir s\'ils ont bien réalisé la tâche. Utile pour une tâche "Captation"',

            'notifBureau': 'Rappel régulièrement au bureau l\'existence de la tâche. Utile pour une tâche "Montage".',
            'notifMembre': "Envoie régulièrement des notifications aux membres inscrits à la tâche tant qu'elle n'est pas terminée. Utile pour une tâche \"Montage\"",
        }


class CreneauForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CreneauForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
        self.fields['debut'].widget.attrs.update(
            {'class': 'form-control datetimepicker-input',
             'data-target': '#id_debut', 'data-toggle': 'datetimepicker'})
        self.fields['fin'].widget.attrs.update(
            {'placeholder': '(Facultatif)',
             'class': 'form-control datetimepicker-input',
             'data-target': '#id_fin', 'data-toggle': 'datetimepicker'})

    class Meta:
        model = Creneau
        fields = ['debut', 'fin', 'maxMembreAssignable']
        help_texts = {
            'debut': "Pour une captation, date de la capta",
            'fin': "Facultatif : Pour une captation, date de fin de la capta.",
            'maxMembreAssignable': "Régler à 0 pour interdire les inscriptions à la tâche.",
        }
