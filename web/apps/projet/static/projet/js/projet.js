$(function () {
    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]'
    });
});

function ajouter_role() {
    select = $('select[id^=select-role]').last();
    select2 = select.clone().attr('id', 'select-role' + (parseInt(select.attr('id').substr(11)) + 1));
    select.parent().append(select2);
}

function diffuser(id_tache) {
    $.ajax({
        url: url_diffuser_tache,
        type: 'post',
        dataType: 'json',
        data: {
            'id_tache': id_tache,
            'csrfmiddlewaretoken': csrf_token
        },
        success: function (response) {
            location.reload();
        }
    });
}

function load_tache(id_tache) {
    $('#chargement').show();
    $('#side-col-content').html('');
    $.ajax({
        url: url_tache_details,
        type: 'post',
        dataType: 'html',
        data: {
            'id_tache': id_tache,
            'csrfmiddlewaretoken': csrf_token
        },
        success: function (response) {
            $('#side-col-content').html(response);
            $('#side-col').addClass('col-tache');
            $('#chargement').hide();
        }

    });
}

function load_description() {
    $('#chargement').show();
    $('#side-col-content').html('');
    $.ajax({
        url: url_projet_description,
        type: 'post',
        dataType: 'html',
        data: {
            'id_projet': id_projet,
            'csrfmiddlewaretoken': csrf_token
        },
        success: function (response) {
            $('#side-col-content').html(response);
            $('#side-col').removeClass('col-tache');
            $('#chargement').hide();
        }
    })
}

function inscription_projet() {
    $.ajax({
        url: url_inscription_projet,
        dataType: 'json',
        type: 'post',
        data: {
            'id_projet': id_projet,
            'id_user': id_user,
            'typeParticipant': JSON.stringify($('select[id^=select-role]').map(function () {
                return $(this).val();
            }).toArray()),
            'csrfmiddlewaretoken': csrf_token,
        },
        success: function (data) {
            if (data['success'])
                location.reload();
            else {
                alert("Erreur : " + data['erreur']);
                location.reload();
            }
        }
    });
}

function inscription_creneau(id_creneau) {
    $.ajax({
        url: url_inscription_creneau,
        dataType: 'json',
        type: 'post',
        data: {
            'id_creneau': id_creneau,
            'id_user': id_user,
            'csrfmiddlewaretoken': csrf_token,
        },
        success: function (data) {
            if (data['success'])
                location.reload();
            else {
                alert("Erreur : " + data['erreur']);
                location.reload();
            }
        }
    });
}

function inscription_tache(id_tache) {
    $.ajax({
        url: url_inscription_tache,
        dataType: 'json',
        type: 'post',
        data: {
            'id_tache': id_tache,
            'id_user': id_user,
            'csrfmiddlewaretoken': csrf_token,
        },
        success: function (data) {
            if (data['success'])
                location.reload();
            else {
                alert("Erreur : " + data['erreur']);
                location.reload();
            }
        }
    });
}

function fiche_cession_droits(id_creneau) {
    $('#myModal button').last().attr('onclick', 'inscription_creneau(' + id_creneau + ')');
    $('#myModal').modal();
}

function fiche_cession_droits_tache(id_tache) {
    $('#myModal button').last().attr('onclick', 'inscription_tache(' + id_tache + ')');
    $('#myModal').modal();
}

function supprimer_creneau(id_creneau) {
    $.ajax({
        url: url_suppression_creneau,
        dataType: 'json',
        type: 'post',
        data: {
            'id_creneau': id_creneau,
            'csrfmiddlewaretoken': csrf_token,
        },
        success: function (data) {
            if (data['success'])
                location.reload();
            else {
                alert("Erreur : " + data['erreur']);
                location.reload();
            }
        }
    });
}

function supprimer_participant_tache(id_tache, id_participant) {
    $.ajax({
        url: url_suppression_participant_tache,
        dataType: 'json',
        type: 'post',
        data: {
            'id_tache': id_tache,
            'id_participant': id_participant,
            'csrfmiddlewaretoken': csrf_token,
        },
        success: function (data) {
            if (data['success'])
                location.reload();
            else {
                alert("Erreur : " + data['erreur']);
                location.reload();
            }
        }
    });
}

function supprimer_participant_creneau(id_creneau, id_participant) {
    $.ajax({
        url: url_suppression_participant_creneau,
        dataType: 'json',
        type: 'post',
        data: {
            'id_creneau': id_creneau,
            'id_participant': id_participant,
            'csrfmiddlewaretoken': csrf_token,
        },
        success: function (data) {
            if (data['success'])
                location.reload();
            else {
                alert("Erreur : " + data['erreur']);
                location.reload();
            }
        }
    });
}

function supprimer_participant_projet(id_projet, id_participant) {
    $.ajax({
        url: url_suppression_participant_projet,
        dataType: 'json',
        type: 'post',
        data: {
            'id_projet': id_projet,
            'id_participant': id_participant,
            'csrfmiddlewaretoken': csrf_token,
        },
        success: function (data) {
            if (data['success'])
                location.reload();
            else {
                alert("Erreur : " + data['erreur']);
                location.reload();
            }
        }
    });
}

function supprimer_tache(id_tache) {
    $.ajax({
        url: url_suppression_tache,
        dataType: 'json',
        type: 'post',
        data: {
            'id_tache': id_tache,
            'csrfmiddlewaretoken': csrf_token,
        },
        success: function (data) {
            if (data['success'])
                location.reload();
            else {
                alert("Erreur : " + data['erreur']);
                location.reload();
            }
        }
    });
}

function terminer_tache(id_tache) {
    $.ajax({
        url: url_terminer_tache,
        dataType: 'json',
        type: 'post',
        data: {
            'id_tache': id_tache,
            'csrfmiddlewaretoken': csrf_token,
        },
        success: function (data) {
            if (data['success'])
                location.reload();
            else {
                alert("Erreur : " + data['erreur']);
                location.reload();
            }
        }
    });
}