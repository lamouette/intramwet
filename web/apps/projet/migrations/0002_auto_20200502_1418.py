# Generated by Django 3.0.5 on 2020-05-02 12:18

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('projet', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='participantprojet',
            name='utilisateur',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='fichiertache',
            name='tache',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='fichiers', to='projet.Tache'),
        ),
        migrations.AddField(
            model_name='fichiertache',
            name='utilisateur',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='creneau',
            name='membreAssigne',
            field=models.ManyToManyField(blank=True, to='projet.ParticipantProjet'),
        ),
        migrations.AddField(
            model_name='creneau',
            name='tache',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='creneaux', to='projet.Tache'),
        ),
    ]
