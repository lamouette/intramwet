from django.urls import path

from .views import accueil

urlpatterns = [
    path('nom_d_utilisateur', accueil, {'modal_nom_utilisateur': True},
         name='accueil'),
    path('emprunt/reussi', accueil, {'modal_emprunt_reussi': True},
         name='accueil'),
    path('register', accueil, {'modal_inscription': True},
         name='accueil'),
    path('', accueil, name='accueil'),
]
