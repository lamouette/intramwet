$(function () {
    loadMap();
    if (isInIframe()) {
        $('a').attr('target', '_blank');
    }
    $('a[href^="#"]').click(function () {
        let the_id = $(this).attr("href");
        if (the_id === '#')
            return;

        $('html, body').animate({
            scrollTop: $(the_id).offset().top
        }, 'slow');

        return false;
    });
    let m = $("#myModal");
    if (m.length) m.modal('show');
});

function loadMap() {
    var map = L.map('plan').setView([mapCenterLatitude, mapCenterLongitude], mapZoomLevel);
    var marker = L.marker([pinLatitude, pinLongitude]).addTo(map);
    marker.bindPopup(popupContent).openPopup();
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        maxZoom: 18
    }).addTo(map);
}

function isInIframe() {
    return window.location !== window.parent.location;
}