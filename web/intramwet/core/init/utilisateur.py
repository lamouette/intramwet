from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Permission, Group

from apps.initializer.base_initializer import BaseInitializer
from apps.initializer.initialization_handler import initialization_handler
from apps.utilisateur.models import Utilisateur, DescriptionGroupe, StatutCA, CotisationGroupe


class UtilisateurInitializer(BaseInitializer):
    priority = 6

    def init_0_su(self):
        Utilisateur.objects.get_or_create(
            username='medialamouette',
            first_name="Média",
            last_name="La Mouette",
            email='bureau@medialamouette.fr',
            password=make_password('test1234'),
            is_superuser=True,
            is_staff=True,
            valide=True,
            actif=True,
            is_active=True
        )

    def init_1_groupes(self):
        p_wiki = Permission.objects.get(codename='acceder_wiki').id
        p_materiel = Permission.objects.get(codename='acceder_materiel').id
        p_local = Permission.objects.get(codename='acceder_local').id
        p_projets = Permission.objects.get(codename='acceder_projets').id
        p_membres = Permission.objects.get(codename='acceder_membres').id
        p_fiche_inscription = Permission.objects.get(codename='fiche_inscription').id
        p_organisation = Permission.objects.get(codename='represente_organisation').id

        # Initialisation des rôles par défaut
        membreVA, _ = Group.objects.get_or_create(name="Membre VA/INSA")
        membreVA.permissions.add(p_projets, p_materiel, p_local,
                               p_fiche_inscription, p_wiki, p_membres)
        membreVA.save()
        membreVA_desc_text = "Si vous faîtes ou souhaitez faire partie de Média la Mouette, et que vous êtes un insalien"
        DescriptionGroupe.objects.get_or_create(group=membreVA,
                                                description=membreVA_desc_text)

        membreAncienINSA, _ = Group.objects.get_or_create(name="Membre Ancien INSA")
        membreAncienINSA.permissions.add(p_projets, p_materiel, p_local,
                                 p_fiche_inscription, p_wiki, p_membres)
        membreAncienINSA.save()
        membreAncienINSA_desc_text = "Si vous faîtes ou souhaitez faire partie de Média la Mouette, et que vous êtes un ancien insalien"
        DescriptionGroupe.objects.get_or_create(group=membreAncienINSA,
                                                description=membreAncienINSA_desc_text)

        membreExterne, _ = Group.objects.get_or_create(
            name="Membre Externe")
        membreExterne.permissions.add(p_projets, p_materiel, p_local,
                                         p_fiche_inscription, p_wiki,
                                         p_membres)
        membreExterne.save()
        membreExterne_desc_text = "Si vous faîtes ou souhaitez faire partie de Média la Mouette, et que vous êtes un externe"
        DescriptionGroupe.objects.get_or_create(group=membreExterne,
                                                description=membreExterne_desc_text)

        cinet, _ = Group.objects.get_or_create(name="Cinéma-études")
        cinet.permissions.add(p_projets, p_materiel, p_local,
                              p_fiche_inscription, p_wiki, p_membres)
        cinet.save()
        cinet_desc_text = "Si vous êtes membre de la section Cinéma-études"
        DescriptionGroupe.objects.get_or_create(group=cinet,
                                                description=cinet_desc_text)

        exterieur, _ = Group.objects.get_or_create(name="Extérieur")
        exterieur.permissions.add(p_materiel, p_organisation, p_wiki, p_membres)
        exterieur.save()
        exterieur_desc_text = "Si vous n'appartenez à aucune organisation"
        DescriptionGroupe.objects.get_or_create(group=exterieur,
                                                description=exterieur_desc_text)

        association, _ = Group.objects.get_or_create(name="Association")
        association.permissions.add(p_materiel, p_projets, p_organisation, p_wiki, p_membres)
        association.save()
        association_desc_text = "Si vous représentez une association INSA"
        DescriptionGroupe.objects.get_or_create(group=association,
                                                description=association_desc_text)

        capsules_videos, _ = Group.objects.get_or_create(
            name="Capsules Vidéos")
        capsules_videos.permissions.add(p_materiel, p_wiki, p_membres)
        capsules_videos.save()
        capsules_videos_desc_text = "Si vous participez au projet Capsules Vidéos"
        DescriptionGroupe.objects.get_or_create(group=capsules_videos,
                                                description=capsules_videos_desc_text)


    def init_2_statut_ca(self):
        StatutCA.objects.get_or_create(statut="Président",
                                       poste=StatutCA.PRESIDENT,
                                       acceder_passation=True,
                                       acceder_treso=True,
                                       acceder_su=True,
                                       unique=True)

        StatutCA.objects.get_or_create(statut="Trésorier",
                                       poste=StatutCA.TRESORIER,
                                       acceder_treso=True,
                                       unique=True)

        StatutCA.objects.get_or_create(statut="Secrétaire",
                                       poste=StatutCA.SECRETAIRE,
                                       acceder_passation=True,
                                       acceder_treso=True,
                                       unique=True)
        StatutCA.objects.get_or_create(statut="Vice Président",
                                       acceder_treso=True,
                                       poste=StatutCA.AUTRE)
        StatutCA.objects.get_or_create(statut="Vice Trésorier",
                                       acceder_treso=True,
                                       poste=StatutCA.AUTRE)
        StatutCA.objects.get_or_create(statut="Vice Secrétaire",
                                       poste=StatutCA.AUTRE)
        StatutCA.objects.get_or_create(statut="Développeur",
                                       acceder_su=True,
                                       poste=StatutCA.DEV)
        StatutCA.objects.get_or_create(statut="Resp Com'",
                                       poste=StatutCA.AUTRE)
        StatutCA.objects.get_or_create(statut="Resp Formations",
                                       poste=StatutCA.AUTRE)
        StatutCA.objects.get_or_create(statut="Ancien bural",
                                       poste=StatutCA.AUTRE)
        StatutCA.objects.get_or_create(statut="Gestionnaire cinet'",
                                       poste=StatutCA.GESTIONNAIRE_CINET)

    def init_3_cotisations(self):
        CotisationGroupe.objects.get_or_create(
            group=Group.objects.get(name="Membre VA/INSA"),
            cotisation=10
        )
        CotisationGroupe.objects.get_or_create(
            group=Group.objects.get(name="Membre Ancien INSA"),
            cotisation=15
        )
        CotisationGroupe.objects.get_or_create(
            group=Group.objects.get(name="Membre Externe"),
            cotisation=50
        )
        CotisationGroupe.objects.get_or_create(
            group=Group.objects.get(name="Cinéma-études"),
            cotisation=0
        )
