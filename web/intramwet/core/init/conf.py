from django.contrib.staticfiles.finders import find
from django.core.files import File

from apps.conf.models import AssociationLegalProperties, AssociationSocialProperties, AssociationGraphics, WebsiteProperties
from apps.initializer.base_initializer import BaseInitializer


class ConfInitializer(BaseInitializer):
    priority = 2

    def init_association_legal_props(self):
        logo_png_path = find('images/logo.png')
        logo_jpg_path = find('images/logo.jpg')
        logo_negatif_png_path = find('images/logo_negatif.png')
        association_articles_path = find('documents/statuts.pdf')
        association_bylaw_path = find('documents/reglement_interieur.pdf')
        with open(logo_png_path, 'rb') as logo_png,\
             open(logo_jpg_path,'rb') as logo_jpg,\
             open(logo_negatif_png_path, 'rb') as logo_negatif_png,\
             open(association_articles_path, 'rb') as association_articles,\
             open(association_bylaw_path, 'rb') as association_bylaw:
            AssociationLegalProperties.objects.create(
                name='Média La Mouette',
                short_name='La Mouette',
                short_description="L'association audiovisuelle de l'INSA Lyon",
                legal_status='Association Loi 1901',
                siren=881623862,
                siret=88162386200011,
                address='INSA Lyon - Casier du BDE\n20 avenue Albert Einstein',
                address_city='Villeurbanne',
                address_postal_code=69621,
                address_postal_code_cedex=True,
                address_country='France',
                association_articles=File(association_articles,
                                          name='statuts.pdf'),
                association_bylaw=File(association_bylaw,
                                       name='reglement_interieur.pdf'),
            )

    def init_association_graphics(self):
        logo_png_path = find('images/logo.png')
        logo_jpg_path = find('images/logo.jpg')
        logo_negatif_png_path = find('images/logo_negatif.png')
        og_image_jpg_path = find('social/og-image.jpg')
        favicon_16_png_path = find('favicon/favicon-16x16.png')
        favicon_32_png_path = find('favicon/favicon-32x32.png')
        favicon_150_png_path = find('favicon/mstile-150x150.png')
        favicon_180_png_path = find('favicon/apple-touch-icon.png')
        favicon_192_png_path = find('favicon/android-chrome-192x192.png')
        favicon_512_png_path = find('favicon/android-chrome-512x512.png')
        favicon_pinned_svg_path = find('favicon/safari-pinned-tab.svg')
        favicon_ico_path = find('favicon/favicon.ico')
        with open(logo_png_path, 'rb') as logo_png,\
             open(logo_jpg_path, 'rb') as logo_jpg,\
             open(logo_negatif_png_path, 'rb') as logo_negatif_png,\
             open(og_image_jpg_path, 'rb') as og_image_jpg,\
             open(favicon_16_png_path, 'rb') as favicon_16_png,\
             open(favicon_32_png_path, 'rb') as favicon_32_png,\
             open(favicon_150_png_path, 'rb') as favicon_150_png,\
             open(favicon_180_png_path, 'rb') as favicon_180_png,\
             open(favicon_192_png_path, 'rb') as favicon_192_png,\
             open(favicon_512_png_path, 'rb') as favicon_512_png,\
             open(favicon_pinned_svg_path, 'rb') as favicon_pinned_svg,\
             open(favicon_ico_path, 'rb') as favicon_ico:
            AssociationGraphics.objects.create(
                logo_png=File(logo_png, name='logo.png'),
                logo_jpg=File(logo_jpg, name='logo.jpg'),
                logo_negatif_png=File(logo_negatif_png, name='logo_negatif.png'),
                og_image_jpg=File(og_image_jpg, name='og_image.jpg'),
                favicon_16_png=File(favicon_16_png, name='favicon_16.png'),
                favicon_32_png=File(favicon_32_png, name='favicon_32.png'),
                favicon_150_png=File(favicon_150_png, name='favicon_150.png'),
                favicon_180_png=File(favicon_180_png, name='favicon_180.png'),
                favicon_192_png=File(favicon_192_png, name='favicon_192.png'),
                favicon_512_png=File(favicon_512_png, name='favicon_512.png'),
                favicon_pinned_svg=File(favicon_pinned_svg, name='favicon_pinned.svg'),
                favicon_ico=File(favicon_ico, name='favicon.ico')
            )

    def init_association_social_props(self):
        AssociationSocialProperties.objects.create(
            contact_email='bureau@medialamouette.fr',
            support_email='support@medialamouette.fr',
            office_address="en bas du bâtiment D, à l'INSA Lyon, 20 avenue Albert Einstein, 69621 Villeurbanne CEDEX",
            office_address_latitude=45.780345,
            office_address_longitude=4.874048,
            permanence='du lundi au vendredi, de 13h à 14h',
            meetup='hebdomadaires',
            youtube_channel='http://www.youtube.com/LaMouetteMedia',
            twitter_account='https://twitter.com/MediaLaMouette',
            facebook_account='https://www.facebook.com/MediaLaMouette/',
            instagram_account='',
            website=''
        )

    def init_website_props(self):
        WebsiteProperties.objects.create(
            intramwet_name='medialamouette.fr',
            intramwet_homepage_activated=True,
            intramwet_equipement_icon='fas fa-video',
            intramwet_project_icon='fas fa-film',
            intramwet_url='https://medialamouette.fr',
            intramwet_hosting_company_name='Microsoft Corporation',
            intramwet_hosting_company_address='**Siège social** \nMicrosoft Corporation \nOne Microsoft Way \nRedmond, WA 98052-6399 \nUSA \n \n**Filiale France** \nMicrosoft France \n39, quai du Président Roosevelt \n92130 Issy-les-Moulineaux \nTéléphone : 09 70 01 90 90 - appel non surtaxé',
            intramwet_hosting_location='France',
            intramwet_map_center_latitude=45.7805,
            intramwet_map_center_longitude=4.8740,
            intramwet_map_zoom_level=17,
        )
